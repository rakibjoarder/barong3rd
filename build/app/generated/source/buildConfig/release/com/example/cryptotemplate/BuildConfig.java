/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.cryptotemplate;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.barong.chankura";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.0.0";
}
