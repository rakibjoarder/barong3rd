import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class SharedPreferencesHelper {

  static Future<String> getSession() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get("session") ?? "0";
  }
  static Future<bool> setSession(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("session", value);
  }

  static Future<String> getEmail() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get("email") ?? "0";
  }
  static Future<bool> setEmail(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("email", value);
  }

  static Future<String> getUid() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get("uid") ?? "0";
  }
  static Future<bool> setUid(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("uid", value);
  }


}