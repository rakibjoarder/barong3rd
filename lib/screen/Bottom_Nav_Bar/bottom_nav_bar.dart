import 'package:crypto_template/screen/market/markets.dart';
import 'package:crypto_template/screen/markets/markets.dart';
import 'package:crypto_template/screen/setting/setting.dart';
import 'package:crypto_template/screen/setting/themes.dart';
import 'package:crypto_template/screen/wallets/T1_BottomNav1_wallet.dart';
import 'package:flutter/material.dart';

class bottomNavBar extends StatefulWidget {
  ThemeBloc themeBloc;
  bottomNavBar({this.themeBloc});

  _bottomNavBarState createState() => _bottomNavBarState(themeBloc);
}

class _bottomNavBarState extends State<bottomNavBar> {
  ThemeBloc _themeBloc;
  _bottomNavBarState(this._themeBloc);
  int currentIndex = 0;

  Widget callPage(int current) {
    switch (current) {
      case 0:
        return market();
        break;
      case 1:
        return markets();
        break;
      case 2:
        return T1_wallet();
      case 3:
        return setting(themeBloc: _themeBloc);
//      case 3:
//        return setting(themeBloc: _themeBloc);
//        break;
      default:
        return new market();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: callPage(currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        // Usar -> "BottomNavigationDotBar"
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(title: Text('Home'), icon: Icon(Icons.home)),
          BottomNavigationBarItem(
              title: Text('Market'), icon: Icon(Icons.account_balance)),
          BottomNavigationBarItem(
              title: Text('Wallets'), icon: Icon(Icons.account_balance_wallet)),
//            BottomNavigationBarItem(title: Text('Wallet',style: TextStyle(color: Colors.black87)),icon: Icon(Icons.account_balance_wallet,color: Colors.black87)),
          BottomNavigationBarItem(
              title: Text('Settings'), icon: Icon(Icons.settings)),
        ],
        currentIndex: currentIndex,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.black54,
        showUnselectedLabels: true,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}
