import 'dart:convert';

import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/screen/market/place_order.dart';
import 'package:crypto_template/screen/setting/setting.dart';
import 'package:crypto_template/screen/stake/stake.dart';
import 'package:crypto_template/screen/utils/screen_aware_size.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:k_chart/flutter_k_chart.dart';
import 'package:k_chart/k_chart_widget.dart';

class overview extends StatefulWidget {
  final bloc;
  overview({Key key, this.bloc}) : super(key: key);
  _overviewState createState() => _overviewState(bloc: bloc);
}

class _overviewState extends State<overview> {
  final bloc;
  _overviewState({Key key, this.bloc});
  List<KLineEntity> datas;
  bool showLoading = true;
  MainState _mainState = MainState.MA;
  bool _volHidden = true;
  SecondaryState _secondaryState = SecondaryState.MACD;
  bool isLine = true;
  bool isChinese = true;
  List<DepthEntity> _bids, _asks;
  @override
  void initState() {
    super.initState();
    getData('1day');
    rootBundle.loadString('assets/depth.json').then((result) {
      final parseJson = json.decode(result);
      Map tick = parseJson['tick'];
      var bids = tick['bids']
          .map((item) => DepthEntity(item[0], item[1]))
          .toList()
          .cast<DepthEntity>();
      var asks = tick['asks']
          .map((item) => DepthEntity(item[0], item[1]))
          .toList()
          .cast<DepthEntity>();
      // initDepth(bids, asks);
    });
  }

  void getData(String period) {
    Future<String> future = getIPAddress('$period');
    future.then((result) {
      Map parseJson = json.decode(result);
      List list = parseJson['data'];
      datas = list
          .map((item) => KLineEntity.fromJson(item))
          .toList()
          .reversed
          .toList()
          .cast<KLineEntity>();
      print(datas);
      DataUtil.calculate(datas);
      showLoading = false;
      setState(() {});
    }).catchError((_) {
      showLoading = false;
      setState(() {});
      print('获取数据失败');
    });
  }

  //获取火币数据，需要翻墙
  Future<String> getIPAddress(String period) async {
    var url =
        'https://api.huobi.br.com/market/history/kline?period=${period ?? '1day'}&size=300&symbol=btcusdt';
    String result;
    var response = await http.get(url);
    if (response.statusCode == 200) {
      result = response.body;
    } else {
      print('Failed getting IP address');
    }
    return result;
  }

  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        padding: EdgeInsets.all(0),
        child: Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                Container(
                  color: colorStyle.background,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Container(
                      color: colorStyle.background,
                      height: 220,
                      width: double.infinity,
                      child: KChartWidget(
                        datas,
                        isLine: isLine,
                        bgColor: [
                          Colors.transparent,
                          Colors.transparent,
                        ],
                        mainState: _mainState,
                        volHidden: _volHidden,
                        secondaryState: _secondaryState,
                        fixedLength: 2,
                        timeFormat: TimeFormat.YEAR_MONTH_DAY,
                        isChinese: isChinese,
                      ),
                    ),
                  ),
                ),
                // Positioned(
                //   top: screenAwareSize(290, context),
                //   child: ,
                // )
              ],
            ),
            Stack(
              alignment: Alignment.topCenter,
              children: [
                Container(
                  height: screenAwareSize(70, context),
                ),
                ClipPath(
                  clipper: ClipIntroScreenFirstPart(),
                  child: Container(
                    color: colorStyle.background,
                    height: screenAwareSize(50, context),
                  ),
                ),
                Positioned(
                  top: screenAwareSize(25, context),
                  child: RaisedButton(
                    child: Text(
                      'Buy / Sell',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 20.0,
                          letterSpacing: 1.0),
                    ),
                    onPressed: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return place_order(
                          item: bloc.list,
                        );
                      }));
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    color: Colors.pink,
                    elevation: 6,
                    padding: EdgeInsets.only(
                        right: screenAwareSize(100, context),
                        left: screenAwareSize(100, context),
                        top: screenAwareSize(10, context),
                        bottom: screenAwareSize(10, context)),
                  ),
                ),
              ],
            ),
            Card(
              margin: EdgeInsets.all(10),
              elevation: 5,
              child: ListTile(
                contentPadding: EdgeInsets.all(10),
                title: Text(
                  'USDN Staking',
                  style: TextStyle(
                      color: colorStyle.background,
                      fontSize: screenAwareSize(17, context),
                      fontWeight: FontWeight.w700),
                ),
                leading: CircleAvatar(
                  child: Icon(Icons.explore, color: colorStyle.whiteBacground),
                  backgroundColor: Colors.blue,
                ),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 6.0),
                  child: Text(
                    'Earn 15% by staking USDN',
                    style: TextStyle(color: Colors.black87),
                  ),
                ),
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => stake()));
                },
              ),
            ),
            Card(
              margin: EdgeInsets.all(10),
              elevation: 5,
              child: ListTile(
                contentPadding: EdgeInsets.all(10),
                title: Text(
                  'Verify Account',
                  style: TextStyle(
                      color: colorStyle.background,
                      fontSize: screenAwareSize(17, context),
                      fontWeight: FontWeight.w700),
                ),
                leading: CircleAvatar(
                  child: Icon(Icons.monetization_on,
                      color: colorStyle.whiteBacground),
                  backgroundColor: Colors.blue,
                ),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 6.0),
                  child: Text(
                    'Verify Account: For maximum security, enable OTP and increase deposit and withdrawal limits.',
                    style: TextStyle(color: Colors.black87),
                  ),
                ),
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => setting()));
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ClipIntroScreenFirstPart extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0.0, size.height - 25);
    path.quadraticBezierTo(
        size.width / 4, size.height, size.width / 2, size.height);
    path.quadraticBezierTo(size.width - (size.width / 4), size.height,
        size.width, size.height - 25);
    path.lineTo(size.width, 0.0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

// class kline extends StatefulWidget {
//   final bloc;
//   kline({Key key, this.bloc}) : super(key: key);
//   _overviewState createState() => _overviewState(bloc: bloc);
// }
//
// class _klineState extends State<overview> {
//   final bloc;
//   _klineState({Key key, this.bloc});
//
//
//
//   @override
//   void initState() {
//     super.initState();
//     getData('1day');
//     rootBundle.loadString('assets/depth.json').then((result) {
//       final parseJson = json.decode(result);
//       Map tick = parseJson['tick'];
//       var bids = tick['bids']
//           .map((item) => DepthEntity(item[0], item[1]))
//           .toList()
//           .cast<DepthEntity>();
//       var asks = tick['asks']
//           .map((item) => DepthEntity(item[0], item[1]))
//           .toList()
//           .cast<DepthEntity>();
//       initDepth(bids, asks);
//     });
//   }
//
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
// //      appBar: AppBar(title: Text(widget.title)),
//       body: ListView(
//         padding: EdgeInsets.all(0),
//         children: <Widget>[
//           Stack(children: <Widget>[
//
//             showLoading
//                 ? Container(
//                     width: double.infinity,
//                     height: 450,
//                     alignment: Alignment.center,
//                     child: CircularProgressIndicator())
//                 : Container(),
//           ]),
//           // buildButtons(),
//           // Container(
//           //   height: 230,
//           //   width: double.infinity,
//           //   child: DepthChart(_bids, _asks),
//           // )
//         ],
//       ),
//     );
//   }
//
//   Widget buildButtons() {
//     return Wrap(
//       alignment: WrapAlignment.spaceEvenly,
//       children: <Widget>[
//         button("分时", onPressed: () => isLine = true),
//         button("k线", onPressed: () => isLine = false),
//         button("MA", onPressed: () => _mainState = MainState.MA),
//         button("BOLL", onPressed: () => _mainState = MainState.BOLL),
//         button("隐藏", onPressed: () => _mainState = MainState.NONE),
//         button("MACD", onPressed: () => _secondaryState = SecondaryState.MACD),
//         button("KDJ", onPressed: () => _secondaryState = SecondaryState.KDJ),
//         button("RSI", onPressed: () => _secondaryState = SecondaryState.RSI),
//         button("WR", onPressed: () => _secondaryState = SecondaryState.WR),
//         button("隐藏副视图", onPressed: () => _secondaryState = SecondaryState.NONE),
//         button(_volHidden ? "显示成交量" : "隐藏成交量",
//             onPressed: () => _volHidden = !_volHidden),
//         button("切换中英文", onPressed: () => isChinese = !isChinese),
//       ],
//     );
//   }
//
//   Widget button(String text, {VoidCallback onPressed}) {
//     return FlatButton(
//         onPressed: () {
//           if (onPressed != null) {
//             onPressed();
//             setState(() {});
//           }
//         },
//         child: Text("$text"),
//         color: Colors.blue);
//   }
//
