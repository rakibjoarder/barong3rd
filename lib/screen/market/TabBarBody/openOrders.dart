import 'dart:async';
import 'dart:convert';

import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:crypto_template/screen/utils/screen_aware_size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;

class openOrders extends StatefulWidget {
  final Widget child;
  final bloc;

  openOrders({Key key, this.child, this.bloc}) : super(key: key);

  _openOrdersState createState() => _openOrdersState(bloc: bloc);
}

class _openOrdersState extends State<openOrders> {
  final bloc;
  _openOrdersState({this.bloc});
  var sessions;
  var orderHistoryList;

  getOrderbook(session, query) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/peatio/public/markets/$query/order-book";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    list = json.decode(response.body);
    return list;
  }

  cancelOrder(session, query) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/peatio/market/orders/$query/cancel";
    var response = await http
        .post(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");

    if (response.statusCode == 201) {
      getData(sessions);
    }
    list = json.decode(response.body);
    return list;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SharedPreferencesHelper.getSession().then((session) {
      getData(session);
      sessions = session;
    });
  }

  getData(session) async {
    orderHistoryList = await getOrderbook(session, bloc.list['id']);
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    double mediaQuery = MediaQuery.of(context).size.width / 2.2;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        StreamBuilder(
          stream: bloc.getState,
          initialData: 0,
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data == 1) {
              getData(sessions);
              if (mounted) {
                Timer(Duration(seconds: 2), () {
                  if (mounted) {
                    setState(() {});
                  }
                  bloc.stateSink.add(0);
                });
              }
            }
            return Container();
          },
        ),
        Container(
          color: Theme.of(context).canvasColor,
          child: Padding(
            padding: const EdgeInsets.only(
                left: 0.0, right: 0.0, top: 7.0, bottom: 7.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text(
                    "Buy Amount",
                    style: TextStyle(
                        color: Theme.of(context).hintColor,
                        fontFamily: "Popins"),
                  ),
                ),
                Text(
                  "Price",
                  style: TextStyle(
                      color: Theme.of(context).hintColor, fontFamily: "Popins"),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Text(
                    "Amount Sell",
                    style: TextStyle(
                        color: Theme.of(context).hintColor,
                        fontFamily: "Popins"),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: 350.0,
              width: mediaQuery,
              child: orderHistoryList != null
                  ? orderHistoryList['bids'].length > 0
                      ? ListView.builder(
                          shrinkWrap: true,
                          padding: EdgeInsets.all(0),
                          primary: false,
                          itemCount: orderHistoryList['bids'].length,
                          itemBuilder: (BuildContext ctx, int i) {
                            return _buyAmount(
                                mediaQuery, orderHistoryList['bids'][i]);
                          },
                        )
                      : Center(
                          child: Text('No data'),
                        )
                  : Center(
                      child: CircularProgressIndicator(),
                    ),
            ),
            Container(
              height: 350.0,
              width: 1.0,
              color: Theme.of(context).canvasColor,
            ),
            Container(
              height: 350.0,
              width: mediaQuery,
              child: orderHistoryList != null
                  ? orderHistoryList['asks'].length > 0
                      ? ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          padding: EdgeInsets.all(0),
                          itemCount: orderHistoryList['asks'].length,
                          itemBuilder: (BuildContext ctx, int i) {
                            return _amountSell(
                                mediaQuery, orderHistoryList['asks'][i]);
                          },
                        )
                      : Center(
                          child: Text('No data'),
                        )
                  : Center(
                      child: CircularProgressIndicator(),
                    ),
            ),
          ],
        )
      ],
    );
  }

  void deleteOrderAlert(id) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Theme.of(context).canvasColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: screenAwareSize(10, context),
                ),
                Icon(
                  FontAwesomeIcons.exclamationTriangle,
                  size: screenAwareSize(40, context),
                  color: Colors.red,
                ),
                SizedBox(
                  height: screenAwareSize(20, context),
                ),
                Text(
                  'Do you really want to cancel this order ?',
                  style: TextStyle(fontFamily: "Popins"),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: screenAwareSize(10, context),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        child: Text('No'),
                        color: Colors.green,
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    SizedBox(
                      width: screenAwareSize(20, context),
                    ),
                    Expanded(
                      child: RaisedButton(
                        child: Text('Yes'),
                        color: Colors.red,
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onPressed: () {
                          cancelOrder(sessions, id);
                          Navigator.of(context).pop();
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
            contentPadding: EdgeInsets.all(15),
          );
        });
  }

  Widget _buyAmount(double _width, item) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 19.0),
      child: InkWell(
        onTap: () {
          deleteOrderAlert(item['id']);
        },
        child: Container(
          width: _width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
//              Padding(
//                padding: const EdgeInsets.only(left: 8.0),
//                child: Text(
//                  item['id'].toString(),
//                  style: TextStyle(
//                      color: Theme.of(context).hintColor,
//                      fontFamily: "Gotik",
//                      fontSize: 15.0),
//                ),
//              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: Text(
                  item['origin_volume'].toString(),
                  style: TextStyle(fontFamily: "Gotik", fontSize: 15.0),
                ),
              ),
              Text(
                item['price'].toString(),
                style: TextStyle(
                    color: Colors.greenAccent,
                    fontWeight: FontWeight.w700,
                    fontFamily: "Gotik",
                    fontSize: 15.0),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _amountSell(double _width, item) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 19.0),
      child: InkWell(
        onTap: () {
          deleteOrderAlert(item['id']);
        },
        child: Container(
          width: _width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                item['price'].toString(),
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.w700,
                    fontFamily: "Gotik",
                    fontSize: 15.0),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: Text(
                  item['origin_volume'].toString(),
                  style: TextStyle(fontFamily: "Gotik", fontSize: 15.0),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
