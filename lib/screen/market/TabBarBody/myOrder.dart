import 'dart:async';
import 'dart:convert';

import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class myOrder extends StatefulWidget {
  final Widget child;
  final bloc;

  myOrder({Key key, this.child, this.bloc}) : super(key: key);

  _myOrderState createState() => _myOrderState(bloc: bloc);
}

class _myOrderState extends State<myOrder> {
  final bloc;

  _myOrderState({this.bloc});

  var orderHistoryList;

  getTrades(session, query) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/peatio/public/markets/$query/trades";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
//    print("Response status: ${response.statusCode}");
//    print("Response Body: ${response.body}");
    list = json.decode(response.body);
    return list;
  }

  getMyOrders(session) async {
    var list;
    var url = "https://trade.chankura.com/api/v2/peatio/market/orders";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    list = json.decode(response.body);
    return list;
  }

  var sessions;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SharedPreferencesHelper.getSession().then((session) {
      getData(session);
      sessions = session;
    });
  }

  var myOrderList = [];
  getData(session) async {
    orderHistoryList = await getTrades(session, bloc.list['id']);
    myOrderList = await getMyOrders(session);
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        StreamBuilder(
          stream: bloc.getState,
          initialData: 0,
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data == 3) {
              getData(sessions);
              if (mounted) {
                Timer(Duration(seconds: 2), () {
                  if (mounted) {
                    setState(() {});
                  }
                  bloc.stateSink.add(0);
                });
              }
            }
            return Container();
          },
        ),
        Container(
          color: Theme.of(context).canvasColor,
          child: Padding(
            padding: const EdgeInsets.only(
                left: 0.0, right: 0.0, top: 7.0, bottom: 7.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Text(
                      "Date",
                      style: TextStyle(
                          color: Theme.of(context).hintColor,
                          fontFamily: "Popins"),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text("Side",
                      style: TextStyle(
                          color: Theme.of(context).hintColor,
                          fontFamily: "Popins"),
                      textAlign: TextAlign.center),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Text("Price",
                        style: TextStyle(
                            color: Theme.of(context).hintColor,
                            fontFamily: "Popins"),
                        textAlign: TextAlign.center),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 15.0),
                    child: Text("State",
                        style: TextStyle(
                            color: Theme.of(context).hintColor,
                            fontFamily: "Popins"),
                        textAlign: TextAlign.right),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Container(
          child: Expanded(
            child: myOrderList != null
                ? myOrderList.length > 0
                    ? ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        padding: EdgeInsets.all(0),
                        itemCount: myOrderList.length,
                        itemBuilder: (BuildContext ctx, int i) {
                          return _orderHistory(myOrderList[i]);
                        },
                      )
                    : Center(
                        child: Text('No data'),
                      )
                : Center(child: Container(child: CircularProgressIndicator())),
          ),
        ),
      ],
    );
  }

  Widget _orderHistory(item) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4),
      child: Container(
        color: item['side'] == 'buy'
            ? Colors.redAccent.shade200.withOpacity(.4)
            : Colors.greenAccent.withOpacity(.4),
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: Text(
                  item['created_at'].toString().split('T')[0],
                  style: TextStyle(
                      color: Colors.black, fontFamily: "Gotik", fontSize: 15.0),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(item['side'],
                  style: TextStyle(
                    fontFamily: "Gotik",
                    fontSize: 15.0,
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.center),
            ),
            Expanded(
              flex: 2,
              child: Text(item['price'],
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontFamily: "Gotik",
                      fontSize: 15.0),
                  textAlign: TextAlign.center),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(right: 15.0),
                child: Text(item['state'],
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontFamily: "Gotik",
                        fontSize: 15.0),
                    textAlign: TextAlign.right),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
