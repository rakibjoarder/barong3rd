import 'dart:async';
import 'dart:convert';

import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class trades extends StatefulWidget {
  final Widget child;
  final bloc;

  trades({Key key, this.child, this.bloc}) : super(key: key);

  _tradesState createState() => _tradesState(bloc: bloc);
}

class _tradesState extends State<trades> {
  final bloc;

  _tradesState({this.bloc});

  var orderHistoryList;
  var sessions;
  getTrades(session, query) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/peatio/public/markets/$query/trades";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    list = json.decode(response.body);
    return list;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SharedPreferencesHelper.getSession().then((session) {
      getData(session);
      sessions = session;
    });
  }

  getData(session) async {
    orderHistoryList = await getTrades(session, bloc.list['id']);

    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        StreamBuilder(
          stream: bloc.getState,
          initialData: 0,
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data == 2) {
              getData(sessions);
              if (mounted) {
                Timer(Duration(seconds: 2), () {
                  if (mounted) {
                    setState(() {});
                  }
                  bloc.stateSink.add(0);
                });
              }
            }
            return Container();
          },
        ),
        Container(
          color: Theme.of(context).canvasColor,
          child: Padding(
            padding: const EdgeInsets.only(
                left: 0.0, right: 0.0, top: 7.0, bottom: 7.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Text(
                      "Date",
                      style: TextStyle(
                          color: Theme.of(context).hintColor,
                          fontFamily: "Popins"),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text("Type",
                      style: TextStyle(
                          color: Theme.of(context).hintColor,
                          fontFamily: "Popins"),
                      textAlign: TextAlign.center),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Text("Price",
                        style: TextStyle(
                            color: Theme.of(context).hintColor,
                            fontFamily: "Popins"),
                        textAlign: TextAlign.center),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Text("Amount",
                        style: TextStyle(
                            color: Theme.of(context).hintColor,
                            fontFamily: "Popins"),
                        textAlign: TextAlign.right),
                  ),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Expanded(
          child: Container(
            child: orderHistoryList != null
                ? orderHistoryList.length > 0
                    ? ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        padding: EdgeInsets.all(0),
                        itemCount: orderHistoryList.length,
                        itemBuilder: (BuildContext ctx, int i) {
                          return _orderHistory(orderHistoryList[i]);
                        },
                      )
                    : Center(
                        child: Text('No data'),
                      )
                : Center(child: Container(child: CircularProgressIndicator())),
          ),
        ),
      ],
    );
  }

  Widget _orderHistory(item) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 19.0),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  item['created_at'].toString().split('T')[0],
                  style: TextStyle(
                      color:
                          Theme.of(context).textSelectionColor.withOpacity(0.4),
                      fontFamily: "Gotik",
                      fontSize: 15.0),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(item['taker_type'],
                  style: TextStyle(
                      fontFamily: "Gotik", fontSize: 15.0, color: Colors.red),
                  textAlign: TextAlign.center),
            ),
            Expanded(
              flex: 2,
              child: Text(item['price'],
                  style: TextStyle(
                      color:
                          Theme.of(context).textSelectionColor.withOpacity(0.4),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Gotik",
                      fontSize: 15.0),
                  textAlign: TextAlign.center),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(right: 5.0),
                child: Text(item['volume'],
                    style: TextStyle(
                        color: Theme.of(context)
                            .textSelectionColor
                            .withOpacity(0.4),
                        fontWeight: FontWeight.w700,
                        fontFamily: "Gotik",
                        fontSize: 15.0),
                    textAlign: TextAlign.right),
              ),
            )
          ],
        ),
      ),
    );
  }
}
