import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:crypto_template/screen/bloc/market_bloc_provider.dart';
import 'package:crypto_template/screen/market/TabBarBody/myOrder.dart';
import 'package:crypto_template/screen/market/TabBarBody/openOrders.dart';
import 'package:crypto_template/screen/market/TabBarBody/overview_page.dart';
import 'package:crypto_template/screen/market/TabBarBody/trades.dart';
import 'package:crypto_template/screen/utils/screen_aware_size.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class market extends StatefulWidget {
  final Widget child;

  market({Key key, this.child}) : super(key: key);

  _marketState createState() => _marketState();
}

class _marketState extends State<market> with SingleTickerProviderStateMixin {
  TabController _tabController;
  bool loadCard = true;
  var session;
  var index = 0;
  checkConnetion() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      return false;
    } else {
      if (mounted) {
        setState(() {
          loadCard = false;
        });
      }
    }
    return true;
  }

  MarketBloc bloc;
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    bloc = MarketBlocProvider.of(context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    bloc.dispos();
    super.dispose();
  }

  /// To set duration initState auto start if FlashSale Layout open
  @override
  void initState() {
    checkConnetion();
    // TODO: implement initState
    _tabController = new TabController(length: 4, vsync: this, initialIndex: 0);
    super.initState();
    SharedPreferencesHelper.getSession().then((sessions) {
      session = sessions;
      getData(sessions);
    });
  }

  getTicker(session) async {
    var list;
    var url = "https://trade.chankura.com/api/v2/peatio/public/markets/tickers";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
//    print("Response Body: ${response.body}");
    list = json.decode(response.body);
    return list;
  }

  getMarket(session) async {
    var list = [];
    var url = "https://trade.chankura.com/api/v2/peatio/public/markets";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
//    print("Response Body: ${response.body}");
    list = json.decode(response.body);
//    print("Response Body: ${list}");
    return list;
  }

  var tickertList;
  var marketList;
  var finalList = [];
  var gainerList = [];
  var loserList = [];
  List<KeyValueModel> _datas = [];

  getData(session) async {
    tickertList = await getTicker(session);
    marketList = await getMarket(session.trim());
    for (int i = 0; i < marketList.length; i++) {
      var list = tickertList[marketList[i]['id']]['ticker'];
      list['id'] = marketList[i]['id'];
      list['name'] = marketList[i]['name'];
      finalList.add(list);
      var combo = marketList[i]['name'] +
          ' ' +
          tickertList[marketList[i]['id']]['ticker']['avg_price'];
      _datas.add(KeyValueModel(key: i, value: combo));
      if (list['price_change_percent'].toString().contains('+')) {
        gainerList.add((list));
      } else {
        loserList.add((list));
      }
    }
    print(finalList);
    if (mounted) {
      setState(() {
        _selectedValue = marketList[0]['name'] +
            ' ' +
            tickertList[marketList[0]['id']]['ticker']['avg_price'];
        bloc.list = finalList[0];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//       appBar: AppBar(
//         brightness: Brightness.dark,
//           centerTitle: true,
//           title: Text("Market",style: TextStyle(color: Colors.white,fontFamily: "Gotik",fontWeight: FontWeight.w600,fontSize: 18.5),),
//           elevation: 0.0,
//           backgroundColor: colorStyle.background,
//         ),

      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: screenAwareSize(580, context),
              width: MediaQuery.of(context).size.width,
              child: finalList != null && finalList.length > 0
                  ? Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 50),
                          color: colorStyle.background,
                          child: _datas.length > 0
                              ? selectDropdown(_datas, '', context)
                              : Container(),
                        ),
                        Container(
                          color: colorStyle.background,
                          padding: EdgeInsets.all(10),
                          child: Container(
                              padding: EdgeInsets.all(2),
                              color: Colors.white,
                              child: Material(
                                color: colorStyle.background,
                                child: TabBar(
                                  controller: _tabController,
                                  indicatorColor: Colors.white,
                                  labelColor: Colors.deepPurpleAccent,
                                  tabs: <Widget>[
                                    new Tab(
                                      child: FittedBox(
                                        child: Text(
                                          "Overview",
                                          style: TextStyle(
                                              fontFamily: "Popins",
                                              color: Colors.white,
                                              fontSize:
                                                  screenAwareSize(9, context)),
                                        ),
                                      ),
                                    ),
                                    new Tab(
                                      child: FittedBox(
                                        child: Text(
                                          "Order book",
                                          style: TextStyle(
                                              fontFamily: "Popins",
                                              color: Colors.white,
                                              fontSize:
                                                  screenAwareSize(9, context)),
                                        ),
                                      ),
                                    ),
                                    new Tab(
                                      child: FittedBox(
                                        child: Text(
                                          "Trades",
                                          style: TextStyle(
                                              fontFamily: "Popins",
                                              color: Colors.white,
                                              fontSize:
                                                  screenAwareSize(9, context)),
                                        ),
                                      ),
                                    ),
                                    new Tab(
                                      child: FittedBox(
                                        child: Text(
                                          "My Orders",
                                          style: TextStyle(
                                              fontFamily: "Popins",
                                              color: Colors.white,
                                              fontSize:
                                                  screenAwareSize(9, context)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                        ),
                        Expanded(
                          child: finalList != null && finalList.length > 0
                              ? TabBarView(
                                  children: <Widget>[
//           favorite(),
                                    overview(bloc: bloc),
                                    openOrders(
                                      bloc: bloc,
                                    ),
                                    trades(bloc: bloc),
                                    myOrder(bloc: bloc),
                                  ],
                                  controller: _tabController,
                                )
                              : Container(),
                        )
                      ],
                    )
                  : Center(
                      child: CircularProgressIndicator(),
                    ),
            )
          ],
        ),
      ),
    );
  }

  var _selectedValue = " ";
  //SelectDropdown dropdown
  selectDropdown(List<KeyValueModel> dropdownList, type, context) {
    return Padding(
      padding: EdgeInsets.only(
          left: screenAwareSize(8, context),
          right: screenAwareSize(8, context),
          top: screenAwareSize(2, context),
          bottom: screenAwareSize(1, context)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Theme(
                    data: ThemeData(canvasColor: colorStyle.background),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        items: dropdownList
                            .map((data) => DropdownMenuItem<String>(
                                  child: FittedBox(
                                      child: Text(
                                    data.value,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: screenAwareSize(18, context)),
                                  )),
                                  value: data.key.toString(),
                                ))
                            .toList(),
                        onChanged: (String value) {
                          bloc.stateSink.add(
                              _tabController.index); //1 - for orderbook, 2-
                          setState(() {
                            _selectedValue = _datas[int.tryParse(value)].value;
                          });
                          index = int.tryParse(value);
                          print(finalList[index]);
                          bloc.list = finalList[index];
                          bloc.getKline(session, finalList[index]['id']);
                        },
                        hint: Text(
                          _selectedValue,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: screenAwareSize(18, context)),
                        ),
                        iconEnabledColor: Colors.white,
                        iconDisabledColor: Colors.white,
                      ),
                    ))
              ],
            ),
          ),
          Container(
            height: 0,
          ),
        ],
      ),
    );
  }
}

//Create a Model class to hold key-value pair data
class KeyValueModel {
  int key;
  String value;

  KeyValueModel({this.key, this.value});
}
