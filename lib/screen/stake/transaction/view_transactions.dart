import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/screen/wallets/tab_wallet/T1_lrm.dart';
import 'package:flutter/material.dart';

class viewTransactions extends StatefulWidget {
  final transactionList;
  viewTransactions({this.transactionList});
  @override
  _viewTransactionsState createState() => _viewTransactionsState(transactionList: transactionList);
}

class _viewTransactionsState extends State<viewTransactions> {
  final transactionList;
  _viewTransactionsState({this.transactionList});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: colorStyle.background,
        centerTitle: true,
        title: Text(
          "Wallet",
          style: TextStyle(color: Colors.white, fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        child: transactionList.length > 0 ?   Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left:15.0,top: 15),
              child: Text("Transactions",
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: 20,
                      letterSpacing: 1.1,
                      fontFamily: "Sans")),
            ),
            Expanded(
              flex: 1,
              child:  ListView.builder(
                itemCount: transactionList.length,
                itemBuilder: (context, ind) {
                  return _card(transactionList[ind]);
                },
              ) ,
            ),
          ],
        ): Container(),
    ),
    );
  }

  /// Widget card transaction
  ///
  Widget _card(TransactionData transactionList) {
    return  Card(
      margin: EdgeInsets.all(8),
      elevation: 4,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  height: 44.0,
                  width: 44.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(70.0)),
                    border: Border.all(color: transactionList.color),
                  ),
                  child: Center(
                    child: Icon(
                      transactionList.icon,
                      color: transactionList.color,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        transactionList.date != null ? transactionList.date.substring(0,10) : transactionList.date.toString()+'',
                        style: TextStyle(
                            fontSize: 16.5,
                            letterSpacing: 1.2,
                            fontFamily: "Sans",
                            color: Colors.blue),
                      ),
                      Text(transactionList.state,
                          style: TextStyle(
                              color: Colors.black54,
                              letterSpacing: 1.1,
                              fontFamily: "Popins",
                              fontSize: 13.0))
                    ],
                  ),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  transactionList.amount,
                  style: TextStyle(
                      fontSize: 19.5,
                      letterSpacing: 1.6,
                      fontFamily: "Sans",
                      fontWeight: FontWeight.w600,
                      color: Colors.black54),
                ),
                Text(transactionList.currency,
                    style: TextStyle(
                        color: Colors.black54,
                        letterSpacing: 1.1,
                        fontFamily: "Popins",
                        fontSize: 13.0))
              ],
            ),
          ],
        ),
      ),
    );
  }


}
