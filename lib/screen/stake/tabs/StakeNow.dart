import 'dart:convert';

import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class StakeNow extends StatefulWidget {
  final Widget child;
  final walletList;

  StakeNow({Key key, this.child, this.walletList}) : super(key: key);

  _StakeNowState createState() => _StakeNowState();
}

class _StakeNowState extends State<StakeNow> {
  var balanceList;
  var depositAddress = '';
  var session = '';
  var responseList;
  TextEditingController otpController = new TextEditingController();
  TextEditingController recipentController = new TextEditingController();
  TextEditingController amountController = new TextEditingController();
  var formKey = new GlobalKey<FormState>();
  var scaffoldKey = new GlobalKey();
  var profit = 0.0;

  withdraw(otp, reciepentAddress, amount, session) async {
    var url = "https://trade.chankura.com/api/v2/peatio/account/withdraws/";
    http.Response response = await http.post(url, headers: {
      "Accept": "application/json",
      "Cookie": session
    }, body: {
      'otp': otp,
      // 'rid': reciepentAddress,
      'rid': '3P2g8vBgjyuXY736qGKMEb8RHYBeWm4XK7G',
      'amount': amount,
      'currency': widget.walletList.id
    });

    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");
    if (response.statusCode == 201) {
      _showSnackbar('Withdraw Complete');
    } else if (response.body.contains('withdraw.not_permitted')) {
      _showSnackbar('Withdraw not permitted');
    } else if (response.body.contains('withdraw.create_error')) {
      _showSnackbar('Withdraw Failed');
    } else if (response.body.contains('withdraw.invalid_otp')) {
      _showSnackbar('Invalid Otp');
    } else if (response.body.contains('withdraw.insufficient_balance')) {
      _showSnackbar('Insufficient Balance');
    } else {
      _showSnackbar('failed');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.walletList.id);
    SharedPreferencesHelper.getSession().then((_session) {
      session = _session;
      getData(_session);
    });
  }

  void _showSnackbar(String text) {
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: new Text(text),
      duration: new Duration(seconds: 2),
      action: SnackBarAction(label: "UNDO", onPressed: () {}),
    ));
  }

  getData(session) async {
    balanceList = await getBalance(widget.walletList.id, session);
    print(balanceList);
    if (mounted) {
      setState(() {});
    }
  }

  getBalance(currency, session) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/peatio/account/balances/$currency";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    if (response.statusCode == 200) {
      list = json.decode(response.body);
      return list;
    } else {
      return [];
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    otpController?.dispose();
    recipentController?.dispose();
    amountController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: colorStyle.background,
        centerTitle: true,
        title: Text(
          "Create USDN Deposit",
          style: TextStyle(color: Colors.white, fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            // balanceList != null && balanceList.length > 0
            //     ? Card(
            //         margin: EdgeInsets.all(10),
            //         elevation: 3,
            //         child: Container(
            //           padding: EdgeInsets.all(20),
            //           width: double.infinity,
            //           child: Center(
            //               child: Text(
            //             "Stake",
            //             style: TextStyle(
            //                 color: Colors.black87,
            //                 fontFamily: "Gotik",
            //                 fontSize: screenAwareSize(20, context),
            //                 fontWeight: FontWeight.w600),
            //           )),
            //         ),
            //       )
            //     : Container(),
            balanceList != null && balanceList.length > 0
                ? Container(
                    width: double.infinity,
                    height: 170.0,
                    decoration: BoxDecoration(
                        color: Theme.of(context).canvasColor,
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: Card(
                      margin: EdgeInsets.all(10),
                      elevation: 3,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 30.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  balanceList['currency']
                                      .toString()
                                      .toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Popins",
                                      fontSize: 15.5),
                                ),
                                Text(
                                  balanceList['balance'],
                                  style: TextStyle(
                                    fontFamily: "Popins",
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Locked",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Popins",
                                      fontSize: 15.5),
                                ),
                                Text(
                                  balanceList['locked'],
                                  style: TextStyle(
                                    fontFamily: "Popins",
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Total",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Popins",
                                      fontSize: 15.5),
                                ),
                                Text(
                                  "0",
                                  style: TextStyle(
                                    fontFamily: "Popins",
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : Container(),
            Container(
              height: 205.0,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Card(
                margin: EdgeInsets.all(10),
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 27.0,
                      ),
                      Form(
                        key: formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Amount (USDN)",
                              style: TextStyle(
                                color: Theme.of(context)
                                    .hintColor
                                    .withOpacity(0.7),
                                fontFamily: "Popins",
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  right: 5.0, bottom: 15.0),
                              child: TextField(
                                controller: amountController,
                                onChanged: (value) {
                                  setState(() {
                                    profit = double.parse(value);
                                  });
                                },
                                decoration: InputDecoration(
                                    hintText: "Enter Amount",
                                    hintStyle: TextStyle(
                                        color: Theme.of(context).hintColor,
                                        fontFamily: "Popins",
                                        fontSize: 15.0)),
                                // validator: (input) {
                                //   if (input.isEmpty) {
                                //     return 'Please enter Amount';
                                //   }
                                // },
                              ),
                            ),
                            // Text(
                            //   "Recipent Address",
                            //   style: TextStyle(
                            //     color: Theme.of(context)
                            //         .hintColor
                            //         .withOpacity(0.7),
                            //     fontFamily: "Popins",
                            //   ),
                            // ),
                            // TextFormField(
                            //   controller: recipentController,
                            //   decoration: InputDecoration(
                            //       hintText: "Enter Recipent Address",
                            //       hintStyle: TextStyle(
                            //           color: Theme.of(context).hintColor,
                            //           fontFamily: "Popins",
                            //           fontSize: 15.0)),
                            //   validator: (input) {
                            //     if (input.isEmpty) {
                            //       return 'Please Recipent Address';
                            //     }
                            //   },
                            // ),
                            // SizedBox(
                            //   height: 5.0,
                            // ),
                            // Text(
                            //   "Otp",
                            //   style: TextStyle(
                            //     color: Theme.of(context)
                            //         .hintColor
                            //         .withOpacity(0.7),
                            //     fontFamily: "Popins",
                            //   ),
                            // ),
                            // TextFormField(
                            //   controller: otpController,
                            //   decoration: InputDecoration(
                            //       hintText: "Enter Otp",
                            //       hintStyle: TextStyle(
                            //           color: Theme.of(context).hintColor,
                            //           fontFamily: "Popins",
                            //           fontSize: 15.0)),
                            //   validator: (input) {
                            //     if (input.isEmpty) {
                            //       return 'Please Recipent Address';
                            //     }
                            //   },
                            // ),
                            // SizedBox(
                            //   height: 20.0,
                            // ),
                            RaisedButton(
                              child: Center(
                                child: Text(
                                  "Invest USDN",
                                  style: TextStyle(
                                      fontFamily: "Popins",
                                      fontSize: 16.0,
                                      letterSpacing: 1.5,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              color: Colors.pink,
                              elevation: 6,
                              padding: EdgeInsets.all(10),
                              onPressed: () {
                                if (formKey.currentState.validate()) {
                                  withdraw(
                                      otpController.value.text,
                                      recipentController.value.text,
                                      amountController.value.text,
                                      session);
                                }
                              },
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 170.0,
              decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Card(
                margin: EdgeInsets.all(10),
                elevation: 3,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 30.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Daily Profit",
                            style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.w700,
                                fontFamily: "Popins",
                                fontSize: 15.5),
                          ),
                          Text(
                            double.parse("${profit * 0.000328}")
                                    .toStringAsFixed(2)
                                    .toString() +
                                " USDN",
                            style: TextStyle(
                              fontFamily: "Popins",
                              color: Colors.black87,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Monthly Profit",
                            style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.w700,
                                fontFamily: "Popins",
                                fontSize: 15.5),
                          ),
                          Text(
                            double.parse("${profit * 0.01}")
                                    .toStringAsFixed(2)
                                    .toString() +
                                " USDN",
                            style: TextStyle(
                              fontFamily: "Popins",
                              color: Colors.black87,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Annual Profit",
                            style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.w700,
                                fontFamily: "Popins",
                                fontSize: 15.5),
                          ),
                          Text(
                            double.parse("${profit * 0.12}")
                                    .toStringAsFixed(2)
                                    .toString() +
                                " USDN",
                            style: TextStyle(
                              fontFamily: "Popins",
                              color: Colors.black87,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            )
          ],
        ),
      ),
    );
  }
}
