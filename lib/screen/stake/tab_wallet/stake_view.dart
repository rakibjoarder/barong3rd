import 'dart:convert';

import 'package:crypto_template/component/AssetsWallet/assetsModel.dart';
import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:crypto_template/screen/stake/tabs/StakeNow.dart';
import 'package:crypto_template/screen/stake/tabs/un_stakenow.dart';
import 'package:crypto_template/screen/utils/screen_aware_size.dart';
import 'package:crypto_template/screen/wallets/tabs/fiat_recieve.dart';
import 'package:crypto_template/screen/wallets/tabs/fiat_send.dart';
import 'package:crypto_template/screen/wallets/transaction/view_transactions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class StakeView extends StatefulWidget {
  @override
  _StakeViewState createState() => _StakeViewState();
}

class _StakeViewState extends State<StakeView> {
  var currentcyList = [];
  List<assetsWallet> walletList = [];
  var balanceList = [];
  var not_permitted_flag = true;
  getDeposit(session) async {
    var list = [];
    var url = "https://trade.chankura.com/api/v2/peatio/account/deposits";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("deposit body: ${response.body}");
    if (response.statusCode == 200) {
      list = json.decode(response.body);
    } else if (response.body.toString().contains('deposit.not_permitted')) {
      not_permitted_flag = false;
    }
    return list;
  }

  getWithdraw(session) async {
    var list = [];
    var url = "https://trade.chankura.com/api/v2/peatio/account/withdraws";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("withraw body: ${response.body}");
    if (response.statusCode == 201) {
      list = json.decode(response.body);
    } else if (response.body.toString().contains('withdraw.not_permitted')) {
      not_permitted_flag = false;
      setState(() {});
    }
    return list;
  }

  //calling currency api
  getCurrency() async {
    var list = [];
    var url = "https://trade.chankura.com/api/v2/peatio/public/currencies";
    var response = await http.get(url);
//    print("Response status: ${response.statusCode}");
    print("currencies Body: ${response.body}");
    list = json.decode(response.body);
    return list;
  }

  //calling balane api
  getbalance(session) async {
    var list = [];
    var url = "https://trade.chankura.com/api/v2/peatio/account/balances/";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
//    print("Response status: ${response.statusCode}");
//    print("Response Body: ${response.body}");
    list = json.decode(response.body);
    return list;
  }

  @override
  void initState() {
    super.initState();
    SharedPreferencesHelper.getSession().then((session) {
      getWalletData(session);
    });
  }

  var iconMap = {
    'eth': 'assets/image/market/eth.png',
    'usd': 'assets/image/market/usd.png',
    'trst': 'assets/image/market/weTrust.png'
  };
  var depositList = [];
  var withdrawList = [];
  var transactionList = [];

  //calling the wallet data
  getWalletData(session) async {
    balanceList = await getbalance(session.trim());
    currentcyList = await getCurrency();
    depositList = await getDeposit(session);
    withdrawList = await getWithdraw(session);
    print(withdrawList);
    for (int i = 0; i < currentcyList.length; i++) {
      for (int j = 0; j < balanceList.length; j++) {
        if (currentcyList[i]['id'] == balanceList[j]['currency']) {
          walletList.add(assetsWallet(
              icon: iconMap.containsKey(currentcyList[i]['id'])
                  ? iconMap[currentcyList[i]['id'].toString().trim()]
                  : currentcyList[i]['icon_url'],
              name: currentcyList[i]['name'],
              priceValue: balanceList[j]['balance'],
              id: balanceList[j]['currency'],
              type: currentcyList[i]['type']));
        }
      }
    }

    for (int i = 0; i < depositList.length; i++) {
      transactionList.add(TransactionData(
          date: depositList[i]['completed_at'],
          icon: depositList[i]['state'] == 'collected'
              ? Icons.check
              : depositList[i]['state'] == 'accepted'
                  ? Icons.done_all
                  : Icons.close,
          amount: depositList[i]['amount'],
          created_at: depositList[i]['created_at'],
          currency: depositList[i]['currency'],
          state: depositList[i]['state'],
          color: depositList[i]['state'] == 'collected'
              ? Colors.greenAccent
              : depositList[i]['state'] == 'accepted'
                  ? Colors.green
                  : Colors.red));
    }

    for (int i = 0; i < withdrawList.length; i++) {
      transactionList.add(TransactionData(
          date: withdrawList[i]['done_at'],
          icon: withdrawList[i]['state'] == 'collected'
              ? Icons.check
              : withdrawList[i]['state'] == 'accepted'
                  ? Icons.done_all
                  : Icons.close,
          amount: withdrawList[i]['amount'],
          created_at: withdrawList[i]['created_at'],
          currency: withdrawList[i]['currency'],
          state: withdrawList[i]['state'],
          color: withdrawList[i]['state'] == 'collected'
              ? Colors.greenAccent
              : withdrawList[i]['state'] == 'accepted'
                  ? Colors.green
                  : Colors.red));
    }

//    SharedPreferencesHelper.getUid().then((uid){
//      getusdBalance(session,uid);
//    });
    if (mounted) {
      setState(() {});
    }
  }

  cardList() {
    List<Widget> widgetList = [];

    for (var i = 0; i < walletList.length; i++) {
      if (walletList[i].name == "Neutrino Dollar") {
        widgetList.add(Padding(
          child: _cardHeader(walletList[i]),
          padding: EdgeInsets.all(8),
        ));
      }
    }

    return widgetList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: not_permitted_flag == true
          ? balanceList.length > 0
              ? Column(
                  children: <Widget>[
                    walletList.length > 0
                        ? Padding(
                            padding: const EdgeInsets.only(top: 5.0),

                            ///
                            /// Credit Card slider
                            ///
                            child: Column(
                              children: cardList(),
                            ),
                          )
                        : Container(),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 15.0, right: 15.0, top: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Transactions",
                              style: TextStyle(
                                  color: Colors.black87,
                                  letterSpacing: 1.1,
                                  fontFamily: "Sans")),
                          InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => viewTransactions(
                                        transactionList: transactionList,
                                      )));
                            },
                            child: Container(
                              height: 37.0,
                              width: 105.0,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30.0)),
                                  border: Border.all(color: Colors.blue)),
                              child: Center(
                                child: Text("View all",
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        letterSpacing: 1.1,
                                        fontFamily: "Popins")),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    ///
                    /// List transaction
                    ///
                    ///
                    Expanded(
                      child: transactionList.length > 0
                          ? ListView.builder(
                              itemCount: transactionList.length,
                              itemBuilder: (context, ind) {
                                return _card(transactionList[ind]);
                              },
                            )
                          : Container(),
                    )
//          _card(Theme.of(context).primaryColor, "03/03/2018", "Completed",
//              "+ 0.010", "ETH", Icons.check),
//          _card(Theme.of(context).primaryColor, "0/03/2018", "Completed",
//              "+ 0.020", "BTC", Icons.check),
                  ],
                )
              : Container(
                  child: Center(child: CircularProgressIndicator()),
                )
          : Container(
              margin: EdgeInsets.all(20),
              child: Center(
                  child: Text(
                'Deposit / Withdraw not permitted. Please verify your account first.',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: screenAwareSize(20, context),
                    color: Colors.red,
                    letterSpacing: .5,
                    wordSpacing: 1),
              )),
            ),
    );
  }

  ///
  /// Widget card transaction
  ///
  Widget _card(TransactionData transactionList) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: 44.0,
                width: 44.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(70.0)),
                  border: Border.all(color: transactionList.color),
                ),
                child: Center(
                  child: Icon(
                    transactionList.icon,
                    color: transactionList.color,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      transactionList.date != null
                          ? transactionList.date.substring(0, 10)
                          : transactionList.date.toString() + '',
                      style: TextStyle(
                          fontSize: 16.5,
                          letterSpacing: 1.2,
                          fontFamily: "Sans",
                          color: Colors.blue),
                    ),
                    Text(transactionList.state,
                        style: TextStyle(
                            color: Colors.black54,
                            letterSpacing: 1.1,
                            fontFamily: "Popins",
                            fontSize: 13.0))
                  ],
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                transactionList.amount,
                style: TextStyle(
                    fontSize: 19.5,
                    letterSpacing: 1.6,
                    fontFamily: "Sans",
                    fontWeight: FontWeight.w600,
                    color: Colors.black54),
              ),
              Text(transactionList.currency,
                  style: TextStyle(
                      color: Colors.black54,
                      letterSpacing: 1.1,
                      fontFamily: "Popins",
                      fontSize: 13.0))
            ],
          ),
        ],
      ),
    );
  }

  ///
  /// Widget credit card transaction
  ///
  Widget _cardHeader(item) {
    return Stack(
      children: <Widget>[
        Container(
          height: 260.0,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xFF141C35), Color(0xFF141C35)],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left: 25.0, right: 25.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Earn",
                      style: TextStyle(
                          fontFamily: "Sans",
                          fontWeight: FontWeight.bold,
                          fontSize: 30.0,
                          letterSpacing: 1.5,
                          color: Colors.white),
                    ),
                    Text(
                      "12 % Annual Yield",
                      style: TextStyle(
                          fontFamily: "Sans",
                          fontWeight: FontWeight.bold,
                          fontSize: 22.0,
                          letterSpacing: 1.5,
                          color: Colors.white),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.8),
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.0))),
                        child: Container(
                            padding: EdgeInsets.all(10),
                            child: !item.icon.toString().contains('http')
                                ? Image.asset(item.icon)
                                : Image.network(item.icon))),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            item.name,
                            style: TextStyle(
                                fontFamily: "Popins",
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),
                          Text(
                            "Mimimum amount to hold: 100 USDN",
                            style: TextStyle(
                                fontFamily: "Popins",
                                letterSpacing: 1,
                                fontSize: 11,
                                fontWeight: FontWeight.w300,
                                color: Colors.white54),
                          ),
                          Text(
                            "Minimal period: 24 hours",
                            style: TextStyle(
                                fontFamily: "Popins",
                                letterSpacing: 1,
                                fontSize: 12,
                                fontWeight: FontWeight.w300,
                                color: Colors.white54),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 0.2,
                  width: double.infinity,
                  color: Colors.white,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    InkWell(
                      child: Text(
                        item.type == 'fiat' ? "Withdraw" : "Stake Now",
                        style: TextStyle(
                            fontFamily: "Popins",
                            letterSpacing: 1.5,
                            fontWeight: FontWeight.w300,
                            color: Colors.white),
                      ),
                      onTap: () {
                        if (item.type == 'fiat') {
                          Navigator.of(context).push(PageRouteBuilder(
                              pageBuilder: (_, __, ___) => new receiveFiat(
                                    walletList: item,
                                  )));
                        } else {
                          Navigator.of(context).push(PageRouteBuilder(
                              pageBuilder: (_, __, ___) => new StakeNow(
                                    walletList: item,
                                  )));
                        }
                      },
                    ),
                    Container(
                      height: 15.0,
                      width: 0.5,
                      color: Colors.white,
                    ),

                    InkWell(
                      child: Text(
                        item.type == 'fiat' ? "Deposit Now" : "Unstake",
                        style: TextStyle(
                            fontFamily: "Popins",
                            letterSpacing: 1.5,
                            fontWeight: FontWeight.w300,
                            color: Colors.white),
                      ),
                      onTap: () {
                        if (item.type == 'fiat') {
                          Navigator.of(context).push(PageRouteBuilder(
                              pageBuilder: (_, __, ___) => new sendFiat(
                                    walletList: item,
                                  )));
                        } else {
                          Navigator.of(context).push(PageRouteBuilder(
                              pageBuilder: (_, __, ___) => new UnStakeNow(
                                    walletList: item,
                                  )));
                        }
                      },
                    ),
//                    Container(
//                      height: 15.0,
//                      width: 0.5,
//                      color: Colors.white,
//                    ),
//                    Text(
//                      "Transaction",
//                      style: TextStyle(
//                          fontFamily: "Popins",
//                          letterSpacing: 1.5,
//                          fontWeight: FontWeight.w300,
//                          color: Colors.white),
//                    )
                  ],
                )
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            height: 170.0,
            width: 170.0,
            decoration: BoxDecoration(
                color: Colors.white10.withOpacity(0.1),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(200.0),
                    topRight: Radius.circular(20.0))),
          ),
        ),
      ],
    );
  }
}

class TransactionData {
  String date;
  String state;
  String currency;
  String amount;
  IconData icon;
  String created_at;
  Color color;
  TransactionData(
      {this.date,
      this.state,
      this.currency,
      this.amount,
      this.icon,
      this.created_at,
      this.color});
}
