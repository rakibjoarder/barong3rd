import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/screen/stake/tab_wallet/stake_view.dart';
import 'package:flutter/material.dart';

class stake extends StatefulWidget {
  final Widget child;

  stake({Key key, this.child}) : super(key: key);

  _stakeState createState() => _stakeState();
}

class _stakeState extends State<stake> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF141C35),
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: colorStyle.background,
        centerTitle: true,
        title: Text(
          "Staking",
          style: TextStyle(color: Colors.white, fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: StakeView(),
    );
  }
}
