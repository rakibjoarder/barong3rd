import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:crypto_template/screen/login/login.dart';
import 'package:crypto_template/screen/setting/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:qr_flutter/qr_flutter.dart';

class setting extends StatefulWidget {
  ///
  /// Get data bloc from
  ///
  ThemeBloc themeBloc;

  setting({Key key, this.themeBloc}) : super(key: key);

  _settingState createState() => _settingState(themeBloc);
}

class _settingState extends State<setting> {
  ///
  //calling otp api
  var barcode = '';
  var email = '';
  var session = '';
  var docNumber, docType, docExpiredDate, verification_code = '';
  bool otpEnabled = false;
  TextEditingController _controller = new TextEditingController();
  TextEditingController _phoneNumberController = new TextEditingController();
  var formBarcodeKey = new GlobalKey<FormState>();
  var formPhoneAddKey = new GlobalKey<FormState>();
  var formPhoneVerifyKey = new GlobalKey<FormState>();
  var formDocumentVerifyKey = new GlobalKey<FormState>();
  var _coutryCode = '+27';
  var scaffoldkey = new GlobalKey();
  var phoneVerifyKey = new GlobalKey<FormState>();
  var phoneOtpFlag = true;
  var phoneVerifiedFlag = false;

  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 400, maxWidth: 300);

    setState(() {
      _image = image;
    });
  }

  generateApiKey({session, algorithm, otp, kid}) async {
    var list;
    var url = "https://trade.chankura.com/api/v2/barong/resource/api_keys";
    var response = await http.post(url,
        body: {'algorithm': algorithm, 'totp_code': otp, 'kid': kid},
        headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
//    list = json.decode(response.body);
  }

  uploadDocument(docNumber, docType, docExpiredDate, _image) async {
    String fileContentBase64 = base64Encode(_image.readAsBytesSync());
    print(fileContentBase64);
    var url = "https://trade.chankura.com/api/v2/barong/resource/documents";
    print(_image);
    //
    // FormData formData = new FormData.from({
    //   "doc_expire": docExpiredDate,
    //   "doc_type": docType,
    //   "doc_number" : docNumber,
    //   "upload" : fileContentBase64
    // });

    var response = await http.post(url, body: {
      "doc_expire": docExpiredDate,
      "doc_type": docType,
      "doc_number": docNumber,
      "upload": fileContentBase64
    }, headers: {
      "Accept": "application/json",
      "Cookie": session
    });
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
  }

  getOtp(session, email) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/barong/resource/otp/generate_qrcode";
    var response = await http.post(url,
        body: {'email': email},
        headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    list = json.decode(response.body);

    if (response.statusCode == 201) {
      if (mounted) {
        setState(() {
          if (list['data']['barcode'] != null) {
//           barcode =  list['data']['barcode'];
            print(list['data']['url']);
            barcode = list['data']['url'].split('secret=')[1];
            print(barcode);
          }
        });
      }
    } else if (response.body.contains('resource.otp.already_enabled')) {
      setState(() {
        otpEnabled = true;
      });
    }
  }

  Timer _timer;
  var _start = 30;
  addPhone(session, phone) async {
    var list;
    var url = "https://trade.chankura.com/api/v2/barong/resource/phones";
    var response = await http.post(url, body: {'phone_number': phone},
//    var url = "http://beta.netlink.exchange/api/v2/barong/resource/phones/send_code";
//    var response = await http.post(url, body: {'phone_number': phone},
        headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    list = json.decode(response.body);

    if (response.statusCode == 201) {
      if (mounted) {
        _start = 30;
        if (mounted) {
          setState(() {
            phoneOtpFlag = false;
          });
        }
        const oneSec = const Duration(seconds: 1);
        _timer = new Timer.periodic(
          oneSec,
          (Timer timer) {
            if (mounted) {
              setState(
                () {
                  if (_start < 1) {
                    timer.cancel();
                  } else {
                    _start = _start - 1;
                  }
                },
              );
            }
          },
        );
      }
    } else if (response.body.contains("resource.phone.invalid_num")) {
      _showSnackbar("Invalid Phone Number");
    } else if (response.body.contains("record.not_found")) {
      _showSnackbar("Phone Number Not Found");
    } else if (response.body.contains("resource.phone.exists")) {
      _showSnackbar("Phone Number already exist");
    } else {
      _showSnackbar(response.body.substring(10));
    }
  }

  resendPhoneCode(session, phone) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/barong/resource/phones/send_code";
    var response = await http.post(url,
        body: {'phone_number': phone},
        headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    list = json.decode(response.body);

    if (response.statusCode == 201) {
      if (mounted) {
        _start = 30;
        if (mounted) {
          setState(() {
            phoneOtpFlag = false;
          });
        }
        const oneSec = const Duration(seconds: 1);
        _timer = new Timer.periodic(
          oneSec,
          (Timer timer) {
            if (mounted) {
              setState(
                () {
                  if (_start < 1) {
                    timer.cancel();
                  } else {
                    _start = _start - 1;
                  }
                },
              );
            }
          },
        );
      }
    } else if (response.body.contains("resource.phone.invalid_num")) {
      _showSnackbar("Invalid Phone Number");
    } else if (response.body.contains("record.not_found")) {
      _showSnackbar("Phone Number Not Found");
    } else if (response.body.contains("resource.phone.exists")) {
      _showSnackbar("Phone Number already exist");
    } else {
      _showSnackbar(response.body.substring(10));
    }
  }

  verifyPhone(session, phone, verification_code) async {
    var list;
    var url = "https://trade.chankura.com/api/v2/barong/resource/phones/verify";
    var response = await http.post(url,
        body: {'phone_number': phone, 'verification_code': verification_code},
        headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    list = json.decode(response.body);

    if (response.statusCode == 201) {
      setState(() {
        phoneVerifiedFlag = true;
      });
    } else if (response.body.contains("resource.phone.invalid_num")) {
      _showSnackbar("Invalid Phone Number");
    } else if (response.body.contains("record.not_found")) {
      _showSnackbar("Phone Number Not Found");
    } else if (response.body.contains("resource.phone.exists")) {
      _showSnackbar("Phone Number already exist");
    } else {
      _showSnackbar(response.body.substring(10));
    }
  }

  verifyOtp(session, code) async {
    var list;
    var url = "https://trade.chankura.com/api/v2/barong/resource/otp/enable";
    var response = await http.post(url,
        body: {'code': code},
        headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    list = json.decode(response.body);

    if (response.statusCode == 201) {
//      if(mounted){
//        setState(() {
//          if(list['data']['barcode'] !=null){
////           barcode =  list['data']['barcode'];
//            print(list['data']['url']);
//            barcode = list['data']['url'].split('secret=')[1];
//            print(barcode);
//          }
//        });
//      }
      setState(() {
        otpEnabled = true;
      });
    }
  }

  logOut(session) async {
    var list;
    var url = "https://trade.chankura.com/api/v2/barong/identity/sessions";
    var response = await http.delete(url,
        headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    if (response.statusCode == 200) {
      Navigator.of(context).pushReplacement(PageRouteBuilder(
          pageBuilder: (_, __, ___) => new login(
                themeBloc: widget.themeBloc,
              )));
    }
  }

  ThemeBloc themeBloc;
  _settingState(this.themeBloc);
  bool theme = true;
  String _img = "assets/image/setting/lightMode.png";
  String darkImage = "assets/image/setting/nightMode.png";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SharedPreferencesHelper.getSession().then((_session) {
      SharedPreferencesHelper.getEmail().then((_email) {
        email = _email;
        session = _session;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller?.dispose();
    _phoneNumberController?.dispose();
  }

  void _showSnackbar(String text) {
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: new Text(text),
      duration: new Duration(seconds: 2),
      action: SnackBarAction(label: "UNDO", onPressed: () {}),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: colorStyle.background,
        centerTitle: true,
        title: Text(
          "Settings",
          style: TextStyle(color: Colors.white, fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      key: scaffoldkey,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            ///
            /// Image header
            ///
//            InkWell(
//              onTap: () {
//
//                if (theme == true && (Theme.of(context).brightness == Brightness.dark)) {
//                  setState(() {
//                    _img = "assets/image/setting/nightMode.png";
//                    darkImage = "assets/image/setting/nightMode.png";
//                    theme = false;
//                  });
//                  themeBloc.selectedTheme.add(_buildLightTheme());
//                } else {
//                  themeBloc.selectedTheme.add(_buildDarkTheme());
//                  setState(() {
//                    theme = true;
//                    _img = "assets/image/setting/lightMode.png";
//                    darkImage = "assets/image/setting/lightMode.png";
//                  });
//                }
//              },
//              child: Padding(
//                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
//                child: Container(
//                  height: 125.0,
//                  width: double.infinity,
//                  decoration: BoxDecoration(
//                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                      image: DecorationImage(
//                          image: AssetImage((Theme.of(context).brightness == Brightness.dark) ? _img : darkImage), fit: BoxFit.cover)),
//                ),
//              ),
//            ),

            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ExpansionTile(
                    title: Text('Generate 2FA Code'),
                    onExpansionChanged: ((value) {
                      if (value) {
                        getOtp(session, email);
                      }
                    }),
                    children: <Widget>[
                      (barcode.length > 2 || otpEnabled == true)
                          ? Center(
                              child: otpEnabled == true
                                  ? Container(
                                      child: Center(
                                        child: Column(
                                          children: <Widget>[
                                            Icon(
                                              Icons.done_outline,
                                              color: Colors.green,
                                              size: 40,
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(18.0),
                                              child:
                                                  Text('OTP Already Enabled'),
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  : Column(
                                      children: <Widget>[
                                        SizedBox(
                                          height: 5,
                                        ),
                                        QrImage(
                                          data: barcode,
                                          backgroundColor: Colors.white,
                                          foregroundColor: Colors.black,
                                          errorCorrectionLevel: 2,
                                          size: 250,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 14.0, bottom: 14),
                                          child: Text(
                                            barcode,
                                            style: TextStyle(fontSize: 15),
                                          ),
                                        ),
                                        RaisedButton(
                                          child: Text(
                                            'Copy Code',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          onPressed: () {
                                            Clipboard.setData(new ClipboardData(
                                                text: barcode));
                                          },
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          color: Colors.pink,
                                          elevation: 6,
                                          padding: EdgeInsets.only(
                                              top: 10,
                                              left: 20,
                                              bottom: 10,
                                              right: 20),
                                        ),
                                        Form(
                                          key: formBarcodeKey,
                                          child: Column(
                                            children: <Widget>[
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(20.0),
                                                child: TextFormField(
                                                  validator: (input) {
                                                    if (input.isEmpty) {
                                                      return 'Please type Otp';
                                                    }
                                                  },
                                                  controller: _controller,
                                                  style: new TextStyle(
                                                      color: Colors.black87),
                                                  textAlign: TextAlign.start,
                                                  keyboardType: TextInputType
                                                      .emailAddress,
                                                  decoration: InputDecoration(
                                                    border:
                                                        new OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.all(15.0),
                                                    filled: true,
                                                    hintText:
                                                        'Password on the authenticator',
                                                    labelStyle: TextStyle(
                                                      color: Colors.blue,
                                                    ),
                                                    hintStyle: TextStyle(
                                                        color: Colors.black87),
                                                  ),
                                                ),
                                              ),
                                              RaisedButton(
                                                child: Text(
                                                  'Submit',
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                color: Colors.pink,
                                                elevation: 6,
                                                padding: EdgeInsets.only(
                                                    top: 10,
                                                    left: 20,
                                                    bottom: 10,
                                                    right: 20),
                                                onPressed: () {
                                                  if (formBarcodeKey
                                                      .currentState
                                                      .validate()) {
                                                    formBarcodeKey.currentState
                                                        .save();
                                                    verifyOtp(
                                                        session,
                                                        _controller.text
                                                            .toString());
                                                  }
                                                },
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                            )
                          : Container(
                              child: SizedBox(
                                child: Text(
                                  'Please Wait....',
                                  style: TextStyle(color: Colors.white),
                                ),
                                height: 100,
                              ),
                            ),
                    ],
                  ),
                  ExpansionTile(
                    title: Text('SMS Authentication'),
                    onExpansionChanged: (value) {
                      if (value == true) {
                        setState(() {
                          phoneOtpFlag = true;
                        });
                        print(value);
                      }
                    },
                    children: phoneVerifiedFlag == false
                        ? <Widget>[
                            phoneOtpFlag
                                ? Form(
                                    key: formPhoneAddKey,
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: <Widget>[
                                              CountryCodePicker(
                                                onChanged: _onCountryChange,
                                                // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                                initialSelection: '+27',
                                                favorite: ['+39', 'FR'],
                                                // optional. Shows only country name and flag
                                                showCountryOnly: false,
                                                showFlag: true,
                                                // optional. Shows only country name and flag when popup is closed.
                                                showOnlyCountryWhenClosed:
                                                    false,
                                                // optional. aligns the flag and the Text left
                                                alignLeft: false,
                                              ),
                                              Expanded(
                                                flex: 8,
                                                child: Padding(
                                                  padding: const EdgeInsets.all(
                                                      10.0),
                                                  child: TextFormField(
                                                    validator: (input) {
                                                      if (input.isEmpty) {
                                                        return 'Phone Number';
                                                      }
                                                    },
                                                    controller:
                                                        _phoneNumberController,
                                                    style: new TextStyle(
                                                        color: Colors.black),
                                                    textAlign: TextAlign.start,
                                                    keyboardType: TextInputType
                                                        .emailAddress,
                                                    decoration: InputDecoration(
                                                      border:
                                                          new OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20),
                                                      ),
                                                      contentPadding:
                                                          EdgeInsets.all(10.0),
                                                      filled: true,
                                                      hintText: 'Phone Number',
                                                      hintStyle: TextStyle(
                                                          color:
                                                              Colors.black87),
                                                      labelStyle: TextStyle(
                                                        color: Colors.blue,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        RaisedButton(
                                          child: Text(
                                            'Send Code',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          color: Colors.pink,
                                          elevation: 6,
                                          padding: EdgeInsets.only(
                                              top: 10,
                                              left: 20,
                                              bottom: 10,
                                              right: 20),
                                          onPressed: () {
                                            if (formPhoneAddKey.currentState
                                                .validate()) {
                                              formPhoneAddKey.currentState
                                                  .save();
                                              addPhone(
                                                  session,
                                                  _coutryCode +
                                                      _phoneNumberController
                                                          .text
                                                          .toString());
                                            }
                                          },
                                        ),
                                        SizedBox(
                                          height: 20,
                                        )
                                      ],
                                    ),
                                  )
                                :
                                //verify code
                                Form(
                                    key: phoneVerifyKey,
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 8,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 10.0),
                                                    child: TextFormField(
                                                      validator: (input) {
                                                        if (input.isEmpty) {
                                                          return 'Enter the 6-digit code we sent you';
                                                        }
                                                      },
                                                      onSaved: (value) {
                                                        verification_code =
                                                            value;
                                                      },
                                                      initialValue: null,
                                                      style: new TextStyle(
                                                          color: Colors.white),
                                                      textAlign:
                                                          TextAlign.start,
                                                      keyboardType:
                                                          TextInputType
                                                              .emailAddress,
                                                      decoration:
                                                          InputDecoration(
                                                        border:
                                                            new OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(20),
                                                        ),
                                                        filled: true,
                                                        fillColor:
                                                            Colors.transparent,
                                                        contentPadding:
                                                            EdgeInsets.all(
                                                                10.0),
                                                        hintText:
                                                            'Enter the 6-digit code we sent you',
                                                        hintStyle: TextStyle(
                                                            color:
                                                                Colors.black87),
                                                        labelStyle: TextStyle(
                                                          color: Colors.blue,
                                                        ),
                                                      ),
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            RaisedButton(
                                                child: Text(
                                                  'Verify',
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                color: Colors.pink,
                                                elevation: 6,
                                                padding: EdgeInsets.only(
                                                    top: 10,
                                                    left: 20,
                                                    bottom: 10,
                                                    right: 20),
                                                onPressed: () {
                                                  if (phoneVerifyKey
                                                      .currentState
                                                      .validate()) {
                                                    phoneVerifyKey.currentState
                                                        .save();
                                                    verifyPhone(
                                                        session,
                                                        _coutryCode +
                                                            _phoneNumberController
                                                                .text
                                                                .toString(),
                                                        verification_code);
                                                  }
                                                }),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            RaisedButton(
                                              child: Text(
                                                _start == 0
                                                    ? 'Resend'
                                                    : 'Resend in ${_start} Seconds',
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              color: Colors.pink,
                                              elevation: 6,
                                              padding: EdgeInsets.only(
                                                  top: 10,
                                                  left: 20,
                                                  bottom: 10,
                                                  right: 20),
                                              onPressed: _start == 0
                                                  ? () {
                                                      if (phoneVerifyKey
                                                          .currentState
                                                          .validate()) {
                                                        phoneVerifyKey
                                                            .currentState
                                                            .save();
                                                        resendPhoneCode(
                                                            session,
                                                            _coutryCode +
                                                                _phoneNumberController
                                                                    .text
                                                                    .toString());
                                                      }
                                                    }
                                                  : null,
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  )
                          ]
                        : <Widget>[
                            Icon(
                              Icons.done_outline,
                              color: Colors.green,
                              size: 40,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(18.0),
                              child: Text('Phone  Verified'),
                            )
                          ],
                  ),
                  //documents
                  ExpansionTile(
                    title: Text('Submit Documents'),
                    children: <Widget>[
                      Form(
                        key: formDocumentVerifyKey,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0,
                                  right: 20.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: 35,
                                    decoration: BoxDecoration(
                                      color: Colors.black26,
                                      //              color: Color(0xFF282E41),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      border: Border.all(
                                        color: colorStyle.primaryColor,
                                        width: 0.15,
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 12.0, right: 12.0, top: 10.0),
                                      child: Theme(
                                        data: ThemeData(
                                            hintColor: Colors.transparent),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: TextFormField(
                                            validator: (input) {
                                              if (input.isEmpty) {
                                                return 'Document Number Required';
                                              }
                                            },
                                            onSaved: (value) {
                                              docNumber = value;
                                            },
                                            style: new TextStyle(
                                                color: Colors.white),
                                            textAlign: TextAlign.start,
                                            keyboardType:
                                                TextInputType.emailAddress,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              contentPadding:
                                                  EdgeInsets.all(0.0),
                                              filled: true,
                                              fillColor: Colors.transparent,
                                              hintText: 'Document Number',
                                              hintStyle: TextStyle(
                                                  color: Colors.white30),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0,
                                  right: 20.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: 35,
                                    decoration: BoxDecoration(
                                      color: Colors.black26,
                                      //              color: Color(0xFF282E41),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      border: Border.all(
                                        color: colorStyle.primaryColor,
                                        width: 0.15,
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 12.0, right: 12.0, top: 10.0),
                                      child: Theme(
                                        data: ThemeData(
                                            hintColor: Colors.transparent),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: TextFormField(
                                            validator: (input) {
                                              if (input.isEmpty) {
                                                return 'Document type required';
                                              }
                                            },
                                            onSaved: (value) {
                                              docType = value;
                                            },
                                            style: new TextStyle(
                                                color: Colors.white),
                                            textAlign: TextAlign.start,
                                            keyboardType:
                                                TextInputType.emailAddress,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              contentPadding:
                                                  EdgeInsets.all(0.0),
                                              filled: true,
                                              fillColor: Colors.transparent,
                                              hintText: 'Document Type',
                                              hintStyle: TextStyle(
                                                  color: Colors.white30),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0,
                                  right: 20.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: 35,
                                    decoration: BoxDecoration(
                                      color: Colors.black26,
                                      //              color: Color(0xFF282E41),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      border: Border.all(
                                        color: colorStyle.primaryColor,
                                        width: 0.15,
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 12.0, right: 12.0, top: 10.0),
                                      child: Theme(
                                        data: ThemeData(
                                            hintColor: Colors.transparent),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: TextFormField(
                                            validator: (input) {
                                              if (input.isEmpty) {
                                                return 'Document Expire date required';
                                              }
                                            },
                                            onSaved: (value) {
                                              docExpiredDate = value;
                                            },
                                            style: new TextStyle(
                                                color: Colors.white),
                                            textAlign: TextAlign.start,
                                            keyboardType:
                                                TextInputType.emailAddress,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              contentPadding:
                                                  EdgeInsets.all(0.0),
                                              filled: true,
                                              fillColor: Colors.transparent,
                                              hintText: 'Document Expired Date',
                                              hintStyle: TextStyle(
                                                  color: Colors.white30),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              child: Center(
                                child: _image == null
                                    ? RaisedButton(
                                        child: Text('Select Image'),
                                        disabledColor: Colors.red,
                                        disabledTextColor: Colors.white,
                                      )
                                    : SizedBox(
                                        height: 150,
                                        child: Image.file(_image),
                                      ),
                              ),
                              onTap: getImage,
                            ),
                            RaisedButton(
                              child: Text('Submit'),
                              onPressed: () {
                                if (formDocumentVerifyKey.currentState
                                    .validate()) {
                                  formDocumentVerifyKey.currentState.save();
                                  if (_image != null) {
                                    uploadDocument(docNumber, docType,
                                        docExpiredDate, _image);
                                  }
                                }
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  ExpansionTile(
                    title: Text(
                      'Log Out',
                      style: TextStyle(color: Colors.black87),
                    ),
                    children: <Widget>[
                      Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 5,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 14.0, bottom: 14),
                              child: Text(
                                "Are you sure you want to Log Out ?",
                                style: TextStyle(
                                    fontSize: 15, color: Colors.black87),
                              ),
                            ),
                            RaisedButton(
                              child: Text(
                                'Log Out',
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                logOut(session);
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              color: Colors.pink,
                              elevation: 6,
                              padding: EdgeInsets.all(10),
                            ),
                            SizedBox(
                              height: 10,
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  // RaisedButton(
                  //   child: Text('CREATE API'),
                  //   onPressed: () {
                  //     generateApiKey(
                  //         session: session,
                  //         algorithm: 'SHA-1',
                  //         kid: '123456',
                  //         otp: '814751');
                  //   },
                  // )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _onCountryChange(CountryCode countryCode) {
    //Todo : manipulate the selected country code here
    _coutryCode = countryCode.toString().trim();
    print(countryCode.toString());
  }

  Widget listSetting(String header, String title) {
    return Padding(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            header,
            style: TextStyle(
                color: Theme.of(context).hintColor,
                fontFamily: "Sans",
                fontSize: 13.0),
          ),
          SizedBox(
            height: 9.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                    fontSize: 17.0,
                    fontFamily: "Popins",
                    letterSpacing: 1.5,
                    fontWeight: FontWeight.w300),
              ),
              Icon(Icons.keyboard_arrow_right)
            ],
          ),
          line()
        ],
      ),
    );
  }

  Widget line() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Container(
        width: double.infinity,
        height: 0.5,
        decoration: BoxDecoration(color: Theme.of(context).hintColor),
      ),
    );
  }

  ///
  /// Change to mode light theme
  ///
  DemoTheme _buildLightTheme() {
    return DemoTheme(
        'light',
        ThemeData(
          brightness: Brightness.light,
          accentColor: colorStyle.primaryColor,
          primaryColor: colorStyle.primaryColor,
          buttonColor: colorStyle.primaryColor,
          cardColor: colorStyle.cardColorLight,
          textSelectionColor: colorStyle.fontColorLight,
          scaffoldBackgroundColor: Color(0xFFFDFDFD),
          canvasColor: colorStyle.whiteBacground,
          dividerColor: colorStyle.iconColorLight,
          hintColor: colorStyle.fontSecondaryColorLight,
        ));
  }

  ///
  /// Change to mode dark theme
  ///
  DemoTheme _buildDarkTheme() {
    return DemoTheme(
        'dark',
        ThemeData(
            brightness: Brightness.dark,
            scaffoldBackgroundColor: colorStyle.background,
            backgroundColor: colorStyle.blackBackground,
            dividerColor: colorStyle.iconColorDark,
            accentColor: colorStyle.primaryColor,
            primaryColor: colorStyle.primaryColor,
            hintColor: colorStyle.fontSecondaryColorDark,
            buttonColor: colorStyle.primaryColor,
            canvasColor: colorStyle.grayBackground,
            cardColor: colorStyle.grayBackground,
            textSelectionColor: colorStyle.fontColorDark,
            textSelectionHandleColor: colorStyle.fontColorDarkTitle));
  }
}
