import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'package:crypto_template/component/style.dart';

class DemoTheme {
  final String name;
  final ThemeData data;

  const DemoTheme(this.name, this.data);
}

class ThemeBloc {
  final Stream<ThemeData> themeDataStream;
  final Sink<DemoTheme> selectedTheme;

  factory ThemeBloc() {
    final selectedTheme = PublishSubject<DemoTheme>();
    final themeDataStream = selectedTheme.distinct().map((theme) => theme.data);
    return ThemeBloc._(themeDataStream, selectedTheme);
  }

  const ThemeBloc._(this.themeDataStream, this.selectedTheme);

  DemoTheme initialTheme() {
    var color = Colors.blue.shade900.value;
    return DemoTheme(
        'initial',
        ThemeData(primarySwatch: MaterialColor(
          color,
          <int, Color>{
            50: Color(color),
            100: Color(color),
            200: Color(color),
            300: Color(color),
            400: Color(color),
            500: Color(color),
            600: Color(color),
            700: Color(color),
            800: Color(color),
            900: Color(color),
          },
        ),
        ));
  }
}

