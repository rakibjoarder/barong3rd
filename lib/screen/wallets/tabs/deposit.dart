import 'dart:convert';

import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:crypto_template/screen/utils/screen_aware_size.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:qr_flutter/qr_flutter.dart';

class deposit extends StatefulWidget {
  final Widget child;
  final walletList;

  deposit({Key key, this.child, this.walletList}) : super(key: key);

  _depositState createState() => _depositState();
}

class _depositState extends State<deposit> {
  var balanceList;
  var depositAddress = '';

  getBalance(currency, session) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/peatio/account/balances/$currency";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    if (response.statusCode == 200) {
      list = json.decode(response.body);
      return list;
    } else {
      return [];
    }
  }

  getDepositAddress(currency, session) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/peatio/account/deposit_address/$currency";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    if (response.statusCode == 200) {
      list = json.decode(response.body);
      return list;
    } else {
      return [];
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.walletList.id);
    SharedPreferencesHelper.getSession().then((session) {
      getData(session);
    });
  }

  getData(session) async {
    balanceList = await getBalance(widget.walletList.id, session);
    var depositAddressList =
        await getDepositAddress(widget.walletList.id, session);
    print(depositAddressList);
    if (depositAddressList.length > 0 &&
        depositAddressList['address'] != null) {
      depositAddress = depositAddressList['address'];
    }
    print(depositAddressList);
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: colorStyle.background,
        centerTitle: true,
        title: Text(
          "Wallet",
          style: TextStyle(color: Colors.white, fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            balanceList != null && balanceList.length > 0
                ? Card(
                    margin: EdgeInsets.all(10),
                    elevation: 3,
                    child: Container(
                      padding: EdgeInsets.all(20),
                      width: double.infinity,
                      child: Center(
                          child: Text(
                        "Deposit",
                        style: TextStyle(
                            color: Colors.black87,
                            fontFamily: "Gotik",
                            fontSize: screenAwareSize(20, context),
                            fontWeight: FontWeight.w600),
                      )),
                    ),
                  )
                : Container(),
            balanceList != null && balanceList.length > 0
                ? Container(
                    width: double.infinity,
                    height: 170.0,
                    decoration: BoxDecoration(
                        color: Theme.of(context).canvasColor,
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: Card(
                      margin: EdgeInsets.all(10),
                      elevation: 3,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 30.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "1 " +
                                      balanceList['currency']
                                          .toString()
                                          .toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Popins",
                                      fontSize: 15.5),
                                ),
                                Text(
                                  balanceList['balance'],
                                  style: TextStyle(
                                    fontFamily: "Popins",
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Locked",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Popins",
                                      fontSize: 15.5),
                                ),
                                Text(
                                  balanceList['locked'],
                                  style: TextStyle(
                                    fontFamily: "Popins",
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Total",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Popins",
                                      fontSize: 15.5),
                                ),
                                Text(
                                  "0",
                                  style: TextStyle(
                                    fontFamily: "Popins",
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : Container(),
            SizedBox(
              height: 20.0,
            ),
            Card(
              margin: EdgeInsets.all(10),
              elevation: 6,
              child: depositAddress != ''
                  ? Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Theme.of(context).canvasColor,
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Column(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              SizedBox(
                                height: 20.0,
                              ),
                              depositAddress != ''
                                  ? QrImage(
                                      data: depositAddress,
                                      version: 3,
                                      backgroundColor: Colors.white,
                                      size: 150,
                                    )
                                  : Container(),
                              SizedBox(
                                height: 15.0,
                              ),
                            ],
                          ),
                          Text(
                            depositAddress,
                            style: TextStyle(
                              color:
                                  Theme.of(context).hintColor.withOpacity(0.7),
                              fontFamily: "Popins",
                            ),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 2.0,
                          ),
                          SizedBox(
                            width: 150,
                            child: RaisedButton(
                              child: Center(
                                child: Text(
                                  "COPY ADDRESS",
                                  style: TextStyle(color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              onPressed: () {
                                Clipboard.setData(
                                    new ClipboardData(text: depositAddress));
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              color: Colors.pink,
                              elevation: 6,
                              padding: EdgeInsets.all(10),
                            ),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                        ],
                      ),
                    )
                  : Container(),
            ),
            SizedBox(
              height: 20.0,
            ),
            SizedBox(
              height: 20.0,
            )
          ],
        ),
      ),
    );
  }
}
