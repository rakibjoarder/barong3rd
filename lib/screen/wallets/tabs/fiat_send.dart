import 'dart:convert';

import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:crypto_template/screen/utils/screen_aware_size.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class sendFiat extends StatefulWidget {
  final Widget child;
  final walletList;

  sendFiat({Key key, this.child, this.walletList}) : super(key: key);

  _sendFiatState createState() => _sendFiatState();
}

class _sendFiatState extends State<sendFiat> {
  var balanceList;
  var formKey = new GlobalKey<FormState>();
  TextEditingController amountController, accountController;
  var sessions;
  var scaffoldKey = new GlobalKey<ScaffoldState>();

  getBalance(currency, session) async {
    var list;
    var url =
        "https://trade.chankura.com/api/v2/peatio/account/balances/$currency";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    if (response.statusCode == 200) {
      list = json.decode(response.body);
      return list;
    } else {
      return [];
    }
  }

  send(accountNumber, amount) async {
    var url = "https://trade.chankura.com/api/v2/peatio/account/beneficiaries";
    http.Response response = await http.post(url, headers: {
      "Accept": "application/json",
      "Cookie": sessions
    }, body: {
      'currency': 'Master Card',
      'name': 'Master Card',
      'data': accountNumber
    });

    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");
    if (response.statusCode == 201) {
      _showSnackbar('Deposit Complete');
    } else if (response.body.contains('withdraw.not_permitted')) {
      _showSnackbar('Withdraw not permitted');
    } else if (response.body.contains('withdraw.create_error')) {
      _showSnackbar('Withdraw Failed');
    } else if (response.body.contains('withdraw.invalid_otp')) {
      _showSnackbar('Invalid Otp');
    } else if (response.body.contains('withdraw.insufficient_balance')) {
      _showSnackbar('Insufficient Balance');
    } else {
      _showSnackbar('failed');
    }
  }

  void _showSnackbar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
      duration: new Duration(seconds: 2),
      action: SnackBarAction(label: "UNDO", onPressed: () {}),
    ));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    amountController?.dispose();
    accountController?.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    amountController = new TextEditingController();
    accountController = new TextEditingController();

    print(widget.walletList.id);
    SharedPreferencesHelper.getSession().then((session) {
      getData(session);
      sessions = session;
    });
  }

  getData(session) async {
    balanceList = await getBalance(widget.walletList.id, session);
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: colorStyle.background,
        centerTitle: true,
        title: Text(
          "Wallet",
          style: TextStyle(color: Colors.white, fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            balanceList != null && balanceList.length > 0
                ? Card(
                    margin: EdgeInsets.all(10),
                    elevation: 3,
                    child: Container(
                      padding: EdgeInsets.all(20),
                      width: double.infinity,
                      child: Center(
                          child: Text(
                        "Deposit",
                        style: TextStyle(
                            color: Colors.black87,
                            fontFamily: "Gotik",
                            fontSize: screenAwareSize(20, context),
                            fontWeight: FontWeight.w600),
                      )),
                    ),
                  )
                : Container(),
            balanceList != null && balanceList.length > 0
                ? Container(
                    width: double.infinity,
                    height: 170.0,
                    decoration: BoxDecoration(
                        color: Theme.of(context).canvasColor,
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: Card(
                      margin: EdgeInsets.all(10),
                      elevation: 3,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 30.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "1 " +
                                      balanceList['currency']
                                          .toString()
                                          .toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Popins",
                                      fontSize: 15.5),
                                ),
                                Text(
                                  balanceList['balance'],
                                  style: TextStyle(
                                    fontFamily: "Popins",
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Locked",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Popins",
                                      fontSize: 15.5),
                                ),
                                Text(
                                  balanceList['locked'],
                                  style: TextStyle(
                                    fontFamily: "Popins",
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Total",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Popins",
                                      fontSize: 15.5),
                                ),
                                Text(
                                  "0",
                                  style: TextStyle(
                                    fontFamily: "Popins",
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : Container(),
            SizedBox(
              height: 20.0,
            ),
            Card(
              margin: EdgeInsets.all(10),
              elevation: 3,
              child: Column(
                children: <Widget>[
                  Form(
                    key: formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Amount",
                            style: TextStyle(
                              color:
                                  Theme.of(context).hintColor.withOpacity(0.7),
                              fontFamily: "Popins",
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(right: 5.0, bottom: 15.0),
                            child: TextFormField(
                              controller: amountController,
                              decoration: InputDecoration(
                                  hintText: "Enter Amount",
                                  hintStyle: TextStyle(
                                      color: Theme.of(context).hintColor,
                                      fontFamily: "Popins",
                                      fontSize: 15.0)),
                              validator: (input) {
                                if (input.isEmpty) {
                                  return 'Please enter Amount';
                                }
                              },
                            ),
                          ),
                          Text(
                            "Account Number",
                            style: TextStyle(
                              color:
                                  Theme.of(context).hintColor.withOpacity(0.7),
                              fontFamily: "Popins",
                            ),
                          ),
                          TextFormField(
                            controller: accountController,
                            decoration: InputDecoration(
                                hintText: "Enter Account Number",
                                hintStyle: TextStyle(
                                    color: Theme.of(context).hintColor,
                                    fontFamily: "Popins",
                                    fontSize: 15.0)),
                            validator: (input) {
                              if (input.isEmpty) {
                                return 'Please Enter Account Number';
                              }
                            },
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          RaisedButton(
                            child: Center(
                              child: Text(
                                "Deposit",
                                style: TextStyle(
                                    fontFamily: "Popins",
                                    fontSize: 16.0,
                                    letterSpacing: 1.5,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            color: Colors.pink,
                            elevation: 6,
                            padding: EdgeInsets.all(10),
                            onPressed: () {
                              if (formKey.currentState.validate()) {
                                send(accountController.value.text,
                                    amountController.value.text);
                              }
                            },
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            SizedBox(
              height: 20.0,
            )
          ],
        ),
      ),
    );
  }
}
