import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/screen/wallets/tab_wallet/T1_lrm.dart';
import 'package:flutter/material.dart';

class T1_wallet extends StatefulWidget {
  final Widget child;

  T1_wallet({Key key, this.child}) : super(key: key);

  _T1_walletState createState() => _T1_walletState();
}

class _T1_walletState extends State<T1_wallet> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF141C35),
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: colorStyle.background,
        centerTitle: true,
        title: Text(
          "Wallets",
          style: TextStyle(color: Colors.white, fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: T1_lrm(),
    );
  }
}
