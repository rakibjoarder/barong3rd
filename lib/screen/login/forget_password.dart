import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/screen/login/login.dart';
import 'package:crypto_template/screen/setting/themes.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class forgetPassword extends StatefulWidget {
  ThemeBloc themeBloc;
  forgetPassword({this.themeBloc});
  @override
  _forgetPasswordState createState() => _forgetPasswordState(themeBloc);
}

class _forgetPasswordState extends State<forgetPassword> {
  ThemeBloc _themeBloc;

  _forgetPasswordState(this._themeBloc);

  @override
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  var _formKey = new GlobalKey<FormState>();

  //sign in function
  resetPassword(email) async {
    var url =
        "https://trade.chankura.com/api/v2/barong/identity/users/password/generate_code";
    http.Response response = await http.post(url, body: {
      'email': email,
      'lang': '',
    });

    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");
    if (response.statusCode == 201) {
      _showSnackbar("A password reset link has been sent in $email");
    } else if (response.body.contains('user_doesnt_exist')) {
      _showSnackbar("User with this email doesn\'t exist");
    } else {
      _showSnackbar('Failed to send reset link');
    }
  }

  void _showSnackbar(String text) {
    final key = _scaffoldKey.currentState;
    key.showSnackBar(new SnackBar(
      content: new Text(text),
      duration: new Duration(seconds: 2),
      action: SnackBarAction(label: "UNDO", onPressed: () {}),
    ));
  }

  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(color: colorStyle.background),
        child: Stack(
          children: <Widget>[
            Container(
              height: double.infinity,
              width: double.infinity,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.only(top: mediaQuery.padding.top + 50.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset("assets/image/applogo.png", height: 90.0),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        elevation: 5,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 20,
                            ),
                            Form(
                              key: _formKey,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: TextFormField(
                                  style: new TextStyle(color: Colors.black),
                                  textAlign: TextAlign.start,
                                  obscureText: false,
                                  controller: _emailController,
                                  keyboardType: TextInputType.emailAddress,
                                  autocorrect: false,
                                  autofocus: false,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Email Can\'t be Empty";
                                    } else {
                                      return null;
                                    }
                                  },
                                  decoration: InputDecoration(
                                      border: new OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      prefixIcon: Icon(
                                        Icons.email,
                                        color: Colors.blue,
                                        size: 20,
                                      ),
                                      filled: true,
                                      contentPadding: EdgeInsets.all(0.0),
                                      labelText: 'Email',
                                      hintStyle: TextStyle(color: Colors.white),
                                      labelStyle: TextStyle(
                                        color: Colors.blue,
                                      )),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0, right: 20.0, top: 0.0),
                              child: Container(
                                height: 50.0,
                                width: double.infinity,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  color: Colors.pink,
                                  elevation: 6,
                                  padding: EdgeInsets.all(10),
                                  child: Text(
                                    "Send Verification Code",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 20.0,
                                        letterSpacing: 1.0),
                                  ),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      resetPassword(
                                          _emailController.value.text);
                                    }
                                  },
                                ),
                              ),
                            ),
                            FlatButton(
                              child: Text(
                                "Back to login",
                                style: TextStyle(
                                    color: Colors.pinkAccent,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16.5,
                                    letterSpacing: 1.2),
                              ),
                              onPressed: () {
                                Navigator.of(context)
                                    .pushReplacement(PageRouteBuilder(
                                        pageBuilder: (_, __, ___) => new login(
                                              themeBloc: _themeBloc,
                                            )));
                              },
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
