import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/screen/login/login.dart';
import 'package:crypto_template/screen/setting/themes.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class signUp extends StatefulWidget {
  ThemeBloc themeBloc;
  signUp({this.themeBloc});
  @override
  _signUpState createState() => _signUpState(themeBloc);
}

class _signUpState extends State<signUp> {
  ThemeBloc _themeBloc;
  _signUpState(this._themeBloc);
  @override
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _verifyCodeController = TextEditingController();
  var scaffoldkey = GlobalKey<ScaffoldState>();
  var formKey = GlobalKey<FormState>();

  //Signup function
  signUp(email, password) async {
    var url = "https://trade.chankura.com/api/v2/barong/identity/users";
    http.Response response = await http.post(url, body: {
      'email': email,
      'password': password,
      'recaptcha_response': "",
      'lang': ""
    });
    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");
    if (response.statusCode == 201) {
      var url =
          "https://trade.chankura.com/api/v2/barong/identity/users/email/generate_code";
      http.Response response =
          await http.post(url, body: {'email': email, 'lang': ""});
      print(response.body);
      print("Response body: ${response.body}");
      if (response.statusCode == 201) {
        Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (_, __, ___) => new login(
                  themeBloc: _themeBloc,
                )));
      }
    } else if (response.body.contains('email.taken')) {
      _showSnackbar('Email is already taken');
    } else if (response.body.contains('password.password.password_strength')) {
      _showSnackbar(
          'Password must be 1 upper case ,1 special and number characters  ');
    }
  }

  //Signup function
  verifyEmail(token) async {
    print(token);
    var url =
        "https://trade.chankura.com/api/v2/barong/identity/users/email/confirm_code";
    http.Response response = await http.post(url, body: {
      'token': token,
    });
    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");
    if (response.statusCode == 201) {
      Navigator.of(context).pushReplacement(PageRouteBuilder(
          pageBuilder: (_, __, ___) => new login(
                themeBloc: _themeBloc,
              )));
    } else {}
  }

  void _showSnackbar(String text) {
    final key = scaffoldkey.currentState;
    key.showSnackBar(new SnackBar(
      content: new Text(text),
      duration: new Duration(seconds: 2),
      action: SnackBarAction(label: "UNDO", onPressed: () {}),
    ));
  }

  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    return Scaffold(
      key: scaffoldkey,
      body: Container(
        height: double.infinity,
        width: double.infinity,

        /// Set Background image in splash screen layout (Click to open code)
        decoration: BoxDecoration(color: colorStyle.background),
        child: Stack(
          children: <Widget>[
            Container(
              height: double.infinity,
              width: double.infinity,
              child: SingleChildScrollView(
                child: Form(
                  key: formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      /// Animation text marketplace to choose Login with Hero Animation (Click to open code)
                      Padding(
                        padding:
                            EdgeInsets.only(top: mediaQuery.padding.top + 50.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset("assets/image/applogo.png",
                                height: 90.0),
                          ],
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.all(10),
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 20.0, top: 35.0),
                                child: _buildTextFeild(
                                    widgetIcon: Icon(
                                      Icons.people,
                                      color: Colors.blue,
                                      size: 20,
                                    ),
                                    controller: _emailController,
                                    hint: 'User Name',
                                    obscure: false,
                                    keyboardType: TextInputType.emailAddress,
                                    textAlign: TextAlign.start),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 20.0, top: 15.0),
                                child: _buildTextFeild(
                                    widgetIcon: Icon(
                                      Icons.email,
                                      color: Colors.blue,
                                      size: 20,
                                    ),
                                    controller: _emailController,
                                    hint: 'Email',
                                    obscure: false,
                                    keyboardType: TextInputType.emailAddress,
                                    textAlign: TextAlign.start),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 20.0, top: 15.0),
                                child: _buildTextFeild(
                                    widgetIcon: Icon(
                                      Icons.vpn_key,
                                      color: Colors.blue,
                                      size: 20,
                                    ),
                                    controller: _passwordController,
                                    hint: 'Password',
                                    obscure: true,
                                    keyboardType: TextInputType.emailAddress,
                                    textAlign: TextAlign.start),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 20.0, top: 15.0),
                                child: _buildTextFeild(
                                    widgetIcon: Icon(
                                      Icons.vpn_key,
                                      color: Colors.blue,
                                      size: 20,
                                    ),
                                    controller: _passwordController,
                                    hint: 'Confirm Password',
                                    obscure: true,
                                    keyboardType: TextInputType.emailAddress,
                                    textAlign: TextAlign.start),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 20.0, top: 15),
                                child: SizedBox(
                                  height: 45,
                                  child: RaisedButton(
                                      child: Center(
                                        child: Text(
                                          "Register",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 20.0,
                                              letterSpacing: 1.0),
                                        ),
                                      ),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      color: Colors.pink,
                                      elevation: 6,
                                      padding: EdgeInsets.all(10),
                                      onPressed: () {
                                        //calling signup function
                                        if (formKey.currentState.validate()) {
                                          signUp(_emailController.value.text,
                                              _passwordController.value.text);
                                        }
                                      }),
                                ),
                              ),
                              FlatButton(
                                child: Text(
                                  "Sign In",
                                  style: TextStyle(
                                      color: Colors.pinkAccent,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16.5,
                                      letterSpacing: 1.2),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pushReplacement(
                                      PageRouteBuilder(
                                          pageBuilder: (_, __, ___) =>
                                              new login(
                                                themeBloc: _themeBloc,
                                              )));
                                },
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextFeild({
    String hint,
    TextEditingController controller,
    TextInputType keyboardType,
    bool obscure,
    String icon,
    TextAlign textAlign,
    Widget widgetIcon,
  }) {
    return Padding(
      padding: const EdgeInsets.only(left: 0.0),
      child: TextFormField(
        style: new TextStyle(color: Colors.black),
        textAlign: TextAlign.start,
        obscureText: obscure,
        controller: controller,
        keyboardType: keyboardType,
        autocorrect: false,
        autofocus: false,
        validator: ((value) {
          if (value.isEmpty) {
            return "Please Enter $hint";
          }
        }),
        decoration: InputDecoration(
            border: new OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            prefixIcon: widgetIcon,
            contentPadding: EdgeInsets.all(10.0),
            labelText: hint,
            filled: true,
            hintStyle: TextStyle(color: Colors.black),
            labelStyle: TextStyle(
              color: Colors.blue,
            )),
      ),
    );
  }
}
