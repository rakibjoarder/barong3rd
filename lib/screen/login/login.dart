import 'dart:convert';

import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:crypto_template/screen/Bottom_Nav_Bar/bottom_nav_bar.dart';
import 'package:crypto_template/screen/login/forget_password.dart';
import 'package:crypto_template/screen/login/signup.dart';
import 'package:crypto_template/screen/setting/themes.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class login extends StatefulWidget {
  ThemeBloc themeBloc;
  login({this.themeBloc});
  @override
  _loginState createState() => _loginState(themeBloc);
}

class _loginState extends State<login> {
  ThemeBloc _themeBloc;

  _loginState(this._themeBloc);

  @override
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  var scaffoldkey = GlobalKey<ScaffoldState>();
  String _email, _pass, _otp = "";
  var session;

  var otpNeeded = false;

  //sign in function
  signIn(email, password, otp) async {
    var url = "https://trade.chankura.com/api/v2/barong/identity/sessions";
    http.Response response = await http.post(url, body: {
      'email': email,
      'password': password,
      'otp_code': otp == "" ? "" : otp,
      'recaptcha_response': ""
    });

    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");

    String rawCookie = response.headers['set-cookie'];
    if (rawCookie != null) {
      int index = rawCookie.indexOf(';');
      var session = (index == -1) ? rawCookie : rawCookie.substring(0, index);
      //storing sessiong
      await SharedPreferencesHelper.setSession(session);
    }

    if (response.statusCode == 200) {
      var userData = json.decode(response.body);
      if (userData != null) {
        //storing email and uid
        await SharedPreferencesHelper.setEmail(userData['email']);
        await SharedPreferencesHelper.setUid(userData['uid']);
      }
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => bottomNavBar(themeBloc: _themeBloc)));
    } else if (response.body.contains("identity.session.invalid_otp")) {
      _showSnackbar('Invalid Otp');
    } else if (response.body.contains("identity.session.missing_otp")) {
      _showSnackbar('Otp Required');
      setState(() {
        otpNeeded = true;
      });
    } else {
      _showSnackbar('Invalid Email or Password');
    }
  }

  void _showSnackbar(String text) {
    final key = scaffoldkey.currentState;
    key.showSnackBar(new SnackBar(
      content: new Text(text),
      duration: new Duration(seconds: 2),
      action: SnackBarAction(label: "UNDO", onPressed: () {}),
    ));
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    return Form(
      key: _formKey,
      child: Scaffold(
        key: scaffoldkey,
        body: Container(
          height: double.infinity,
          width: double.infinity,

          /// Set Background image in splash screen layout (Click to open code)
          decoration: BoxDecoration(color: colorStyle.background),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding:
                            EdgeInsets.only(top: mediaQuery.padding.top + 50.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset("assets/image/applogo.png",
                                height: 90.0),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 20,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: TextFormField(
                                  validator: (input) {
                                    if (input.isEmpty) {
                                      return 'Please typle an email';
                                    } else {
                                      return null;
                                    }
                                  },
                                  onSaved: (input) => _email = input,
                                  style: new TextStyle(color: Colors.black),
                                  textAlign: TextAlign.start,
                                  controller: _emailController,
                                  keyboardType: TextInputType.emailAddress,
                                  autocorrect: false,
                                  autofocus: false,
                                  decoration: InputDecoration(
                                      border: new OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      prefixIcon: Icon(
                                        Icons.email,
                                        color: Colors.blue,
                                        size: 20,
                                      ),
                                      contentPadding: EdgeInsets.all(10.0),
                                      labelText: 'Email',
                                      filled: true,
                                      hintStyle: TextStyle(color: Colors.black),
                                      labelStyle: TextStyle(
                                        color: Colors.blue,
                                      )),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10, top: 10),
                                child: TextFormField(
                                  validator: (input) {
                                    if (input.isEmpty) {
                                      return 'Please typle an password';
                                    } else {
                                      return null;
                                    }
                                  },
                                  onSaved: (input) => _pass = input,
                                  style: new TextStyle(color: Colors.black),
                                  textAlign: TextAlign.start,
                                  controller: _passwordController,
                                  keyboardType: TextInputType.emailAddress,
                                  autocorrect: false,
                                  obscureText: true,
                                  autofocus: false,
                                  decoration: InputDecoration(
                                      border: new OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      prefixIcon: Icon(
                                        Icons.vpn_key,
                                        size: 20,
                                        color: Colors.blue,
                                      ),
                                      filled: true,
                                      contentPadding: EdgeInsets.all(0.0),
                                      labelText: 'Password',
                                      hintStyle: TextStyle(color: Colors.white),
                                      labelStyle: TextStyle(
                                        color: Colors.blue,
                                      )),
                                ),
                              ),
                              otpNeeded == true
                                  ? Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10.0, right: 10, top: 20),
                                      child: TextFormField(
                                        validator: (input) {
                                          if (input.isEmpty) {
                                            return 'Please type Otp';
                                          } else {
                                            return null;
                                          }
                                        },
                                        onSaved: (input) => _otp = input,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        textAlign: TextAlign.start,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        autocorrect: false,
                                        obscureText: true,
                                        autofocus: false,
                                        decoration: InputDecoration(
                                            border: new OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            prefixIcon: Icon(
                                              Icons.security,
                                              size: 20,
                                              color: Colors.blue,
                                            ),
                                            contentPadding: EdgeInsets.all(0.0),
                                            filled: true,
                                            labelText: 'Otp ',
                                            hintStyle:
                                                TextStyle(color: Colors.white),
                                            labelStyle: TextStyle(
                                              color: Colors.blue,
                                            )),
                                      ),
                                    )
                                  : Container(),

                              ///
                              /// forgot password
                              ///
                              ///
                              ///
                              FlatButton(
                                child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      "Forget Password ?",
                                      style: TextStyle(
                                        color: Colors.blue,
                                        fontSize: 12.0,
                                      ),
                                    )),
                                onPressed: () {
                                  Navigator.of(context).pushReplacement(
                                      PageRouteBuilder(
                                          pageBuilder: (_, __, ___) =>
                                              forgetPassword(
                                                themeBloc: _themeBloc,
                                              )));
                                },
                              ),

                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 20.0, top: 0.0),
                                child: SizedBox(
                                  width: double.infinity,
                                  height: 45.0,
                                  child: RaisedButton(
                                    onPressed: () {
                                      final formState = _formKey.currentState;
                                      if (formState.validate()) {
                                        formState.save();
                                        //calling signIn function
                                        signIn(
                                            _emailController.value.text,
                                            _passwordController.value.text,
                                            _otp);
                                      }
                                    },
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    color: Colors.pink,
                                    elevation: 6,
                                    padding: EdgeInsets.all(10),
                                    child: Text(
                                      "Sign In",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 20.0,
                                          letterSpacing: 1.0),
                                    ),
                                  ),
                                ),
                              ),
                              FlatButton(
                                child: Text(
                                  "Create Account",
                                  style: TextStyle(
                                      color: Colors.pinkAccent,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16.5,
                                      letterSpacing: 1.2),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pushReplacement(
                                      PageRouteBuilder(
                                          pageBuilder: (_, __, ___) =>
                                              new signUp(
                                                themeBloc: _themeBloc,
                                              )));
                                },
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
