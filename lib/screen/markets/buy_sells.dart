import 'dart:convert';

import 'package:crypto_template/component/style.dart';
import 'package:crypto_template/helper/sharedpreference_helper.dart';
import 'package:crypto_template/screen/utils/screen_aware_size.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class buy_sells extends StatefulWidget {
  final item;

  buy_sells({Key key, this.item}) : super(key: key);

  _buy_sellsState createState() => _buy_sellsState(item: item);
}

enum SingingCharacter { sell, buy }

// ...

SingingCharacter orderType = SingingCharacter.buy;

class _buy_sellsState extends State<buy_sells> {
  final item;
  _buy_sellsState({this.item});
  var session;
  var scaffoldkey = GlobalKey<ScaffoldState>();
  var availableBalance = '';

  var orderCurrency = '';
  var amountCurr = '';
  var priceCurr = '';
  var approxCurr = '';
  var totalCurr = '';
  var side = 'buy';

  sell(market, price, ord_type, volume) async {
    print(side);
    print(market);
    print(price);
    print(ord_type);
    print(volume);
    var url = "https://trade.chankura.com/api/v2/peatio/market/orders";
    http.Response response = await http.post(url, headers: {
      "Accept": "application/json",
      "Cookie": session
    }, body: {
      'market': market,
      'price': price,
      'side': side.trim(),
      'ord_type': ord_type,
      'volume': volume
    });

    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");

    if (response.statusCode == 201) {
      _showSnackbar("Order Placed");
    } else {
      _showSnackbar(response.body);
    }
  }

  void _showSnackbar(String text) {
    final key = scaffoldkey.currentState;
    key.showSnackBar(new SnackBar(
      content: new Text(text),
      duration: new Duration(seconds: 2),
      action: SnackBarAction(label: "UNDO", onPressed: () {}),
    ));
  }

  var formKey = GlobalKey<FormState>();
  TextEditingController priceController,
      amountController,
      totalController,
      orderTypeControlelr;
  var approximatePay = 0.0;
  var boolOrderType = false;
  final formatter = new NumberFormat("#.####");

  buyData() async {
    orderCurrency = item['market'].toString().substring(0, 3).toUpperCase();
    amountCurr = item['market'].toString().substring(0, 3).toUpperCase();
    priceCurr = item['market'].toString().substring(3, 6).toUpperCase();
    approxCurr = item['market'].toString().substring(3, 6).toUpperCase();
    totalCurr = item['market'].toString().substring(3, 6).toUpperCase();
    side = 'buy';
    await getData(session, orderCurrency.toString().toLowerCase());
  }

  sellData() async {
    orderCurrency = item['market'].toString().substring(3, 6).toUpperCase();
    amountCurr = item['market'].toString().substring(0, 3).toUpperCase();
    priceCurr = item['market'].toString().substring(3, 6).toUpperCase();
    approxCurr = item['market'].toString().substring(3, 6).toUpperCase();
    totalCurr = item['market'].toString().substring(3, 6).toUpperCase();
    side = 'sell';
    await getData(session, orderCurrency.toString().toLowerCase());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SharedPreferencesHelper.getSession().then((_session) {
      session = _session;
      var curr = item['market'].toString().substring(0, 3);
      if (item['side'] == 'buy') {
        buyData();
      } else {
        sellData();
      }
    });
    priceController = TextEditingController();
    amountController = TextEditingController();
    totalController = TextEditingController();
    orderTypeControlelr = TextEditingController();

    amountController.text = item['origin_volume'];
    priceController.text = item['price'];
    orderType =
        item['side'] == 'buy' ? SingingCharacter.buy : SingingCharacter.sell;
    side = item['side'] == 'buy' ? 'buy' : 'sell';
    var total = (double.parse(amountController.text) *
            double.parse(priceController.text))
        .toString();
    totalController.text = formatter.parse(total).toString();
    approximatePay = (double.parse(amountController.text) *
            double.parse(priceController.text)) +
        (double.parse(amountController.text) *
                double.parse(priceController.text)) *
            0.02;

    amountController.addListener(() {
      if (boolOrderType == true) {
        if (priceController.text.length > 0) {
          try {
            totalController.text = (double.parse(amountController.text) *
                    double.parse(priceController.text))
                .toString();
          } catch (e) {}
        }
      } else {
        try {
          totalController.text =
              (double.parse(amountController.text) * item['avg_price'])
                  .toString();
        } catch (e) {}
      }
    });
    priceController.addListener(() {
      print(amountController.text.length);
      if (amountController.text.length > 0) {
        try {
          var total = (double.parse(amountController.text) *
                  double.parse(priceController.text))
              .toString();
          totalController.text = formatter.parse(total).toString();
        } catch (e) {}
      }
    });

    totalController.addListener(() {
      if (mounted) {
        setState(() {
          try {
            approximatePay = (double.parse(amountController.text) *
                    double.parse(priceController.text)) +
                (double.parse(amountController.text) *
                        double.parse(priceController.text)) *
                    0.02;
          } catch (e) {}
        });
      }
    });
  }

  var sessions;
  var orderHistoryList;

  //calling balane api
  getbalance(session) async {
    var list = [];
    var url = "https://trade.chankura.com/api/v2/peatio/account/balances/";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("Response Body: ${response.body}");
    list = json.decode(response.body);
    return list;
  }

  getData(session, currency) async {
    var balanceList = await getbalance(session);
    for (int j = 0; j < balanceList.length; j++) {
      if (balanceList[j]['currency'] == currency) {
        availableBalance = balanceList[j]['balance'];
      }
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    priceController.dispose();
    amountController.dispose();
    totalController.dispose();
    orderTypeControlelr.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double mediaQuery = MediaQuery.of(context).size.width / 2.2;
    print(item);
    var grayText = TextStyle(
        color: Theme.of(context).hintColor,
        fontFamily: "Popins",
        fontSize: 12.5);

    var styleValueChart = TextStyle(
        color: Theme.of(context).hintColor,
        fontFamily: "Popins",
        fontSize: 11.5);
    return Scaffold(
      key: scaffoldkey,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: colorStyle.background,
        title: Text(
          item['market'].toString().toUpperCase().substring(0, 3) +
              '/' +
              item['market'].toString().toUpperCase().substring(3, 6), // name
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              color: colorStyle.background,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        'Buy',
                        style: TextStyle(color: Colors.white),
                      ),
                      Radio(
                        value: SingingCharacter.buy,
                        groupValue: orderType,
                        activeColor: Colors.white,
                        onChanged: (SingingCharacter value) {
                          if (mounted) {
                            setState(() {
                              orderType = value;
                              buyData();
                            });
                          }
                        },
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        'Sell',
                        style: TextStyle(color: Colors.white),
                      ),
                      Radio(
                        value: SingingCharacter.sell,
                        activeColor: Colors.white,
                        groupValue: orderType,
                        onChanged: (SingingCharacter value) {
                          if (mounted) {
                            setState(() {
                              orderType = value;
                              sellData();
                            });
                          }
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 20.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            side.toUpperCase() + ' : ' + orderCurrency,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: screenAwareSize(18, context)),
                          ),
                          Text(
                            'Available : $availableBalance',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: screenAwareSize(18, context)),
                          ),
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.all(20),
                    child: Card(
                      elevation: 5,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 20.0),
                            child: orderTypeDropdown(),
                          ),
                          boolOrderType == true
                              ? Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 20.0),
                                  child: _buildTextFeild(
                                      widgetIcon: Icon(
                                        Icons.monetization_on,
                                        color: colorStyle.background,
                                        size: 20,
                                      ),
                                      controller: priceController,
                                      hint: priceCurr + ' Price ',
                                      obscure: false,
                                      keyboardType: TextInputType.number,
                                      textAlign: TextAlign.start),
                                )
                              : Container(),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 20.0),
                            child: _buildTextFeild(
                                widgetIcon: Icon(
                                  Icons.fiber_manual_record,
                                  color: colorStyle.background,
                                  size: 20,
                                ),
                                controller: amountController,
                                hint: amountCurr + ' Amount ',
                                obscure: false,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.start),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 20.0),
                            child: _buildTextFeild(
                                widgetIcon: Icon(
                                  Icons.attach_money,
                                  color: colorStyle.background,
                                  size: 20,
                                ),
                                controller: totalController,
                                hint: totalCurr + ' Total',
                                obscure: false,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.start),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0, right: 20.0, top: 20.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text('Fee:'),
                                  Text('100% x 0.2 = 0.02'),
                                ],
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0, right: 20.0, top: 20.0),
                              child: Column(
                                children: <Widget>[
                                  Text('You will approximately pay'),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      formatter.format(approximatePay) +
                                          ' ' +
                                          approxCurr,
                                      style: TextStyle(
                                          fontSize:
                                              screenAwareSize(20, context),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              )),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20.0, right: 20.0, top: 20),
                    child: SizedBox(
                      height: 50,
                      child: RaisedButton(
                          child: Center(
                            child: Text(
                              side.toUpperCase(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 20.0,
                                  letterSpacing: 1.0),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          color: Colors.pink,
                          elevation: 6,
                          padding: EdgeInsets.only(
                              right: screenAwareSize(100, context),
                              left: screenAwareSize(100, context),
                              top: screenAwareSize(10, context),
                              bottom: screenAwareSize(10, context)),
                          onPressed: () {
//                            calling signup function
                            if (formKey.currentState.validate()) {
                              formKey.currentState.save();
                              sell(item['market'], priceController.value.text,
                                  dropdownValue, amountController.value.text);
                            }
                          }),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTextFeild({
    String hint,
    TextEditingController controller,
    TextInputType keyboardType,
    bool obscure,
    String icon,
    TextAlign textAlign,
    Widget widgetIcon,
  }) {
    return Column(
      children: <Widget>[
        Container(
          height: 53.5,
          decoration: BoxDecoration(
            color: Colors.black26.withOpacity(.08),
//              color: Color(0xFF282E41),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            border: Border.all(
              color: colorStyle.background,
              width: 0.15,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left: 12.0, right: 12.0, top: 10.0),
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: TextFormField(
                style: new TextStyle(color: Colors.black),
                textAlign: textAlign,
                obscureText: obscure,
                controller: controller,
                keyboardType: keyboardType,
                autocorrect: false,
                autofocus: false,
                validator: ((value) {
                  if (value.isEmpty) {
                    return "Please Enter $hint";
                  }
                }),
                decoration: InputDecoration(
                    border: InputBorder.none,
                    icon: Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: widgetIcon,
                    ),
                    contentPadding: EdgeInsets.all(0.0),
                    filled: true,
                    fillColor: Colors.transparent,
                    labelText: hint,
                    hintStyle: TextStyle(color: colorStyle.background),
                    labelStyle: TextStyle(
                      color: colorStyle.background,
                    )),
              ),
            ),
          ),
        ),
      ],
    );
  }

  // Event Dropdown Catogories list
  var _categories = [
    'limit',
    'market',
  ];
  var dropdownValue;

  //Event Category dropdown
  orderTypeDropdown() {
    return FormField<String>(
      validator: (value) {
        if (value == null) {
          return "Select Order Type";
        }
      },
      onSaved: (value) {
        orderTypeControlelr.text = value;
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              height: 53.5,
              decoration: BoxDecoration(
                color: Colors.black26.withOpacity(.08),
//              color: Color(0xFF282E41),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                border: Border.all(
                  color: colorStyle.background,
                  width: 0.15,
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 20.0),
                    child: Icon(
                      Icons.category,
                      color: colorStyle.background,
                    ),
                  ),
                  SizedBox(
                    width: screenAwareSize(8, context),
                  ),
                  Expanded(
                    flex: 9,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                          hint: Text('Order Type'),
                          isDense: true,
                          value: dropdownValue,
                          items: _categories.map((String item) {
                            return DropdownMenuItem<String>(
                              child: Text(item),
                              value: item,
                            );
                          }).toList(),
                          onChanged: (value) {
                            state.didChange(value);
                            if (mounted) {
                              setState(() {
                                dropdownValue = value;
                                if (value == 'market') {
                                  boolOrderType = false;
                                } else {
                                  boolOrderType = true;
                                }
                              });
                            }
                          }),
                    ),
                  )
                ],
              ),
            ),
            state.hasError
                ? SizedBox(height: 5.0)
                : Container(
                    height: 0,
                  ),
            state.hasError
                ? Text(
                    state.errorText,
                    style: TextStyle(
                        color: Colors.redAccent.shade700, fontSize: 12.0),
                  )
                : Container(),
          ],
        );
      },
    );
  }
}
