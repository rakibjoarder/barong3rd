import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class MarketBloc extends BaseBloc {
  var list;
  final stateController = BehaviorSubject<int>();
  Stream<int> get getState => stateController.stream;
  StreamSink<int> get stateSink => stateController.sink;

  MarketBloc() {
    print(list);
  }

  getKline(session, query) async {
    print("KLINE Body: query");
    var list;
    // var url =
    //     "https://trade.chankura.com/api/v2/peatio/public/market/$query/k-line?period=10080";
    var url =
        "https://api.huobi.br.com/market/history/kline?period=${'1day'}&size=300&symbol=btcusdt";
    var response = await http
        .get(url, headers: {"Accept": "application/json", "Cookie": session});
    print("Response status: ${response.statusCode}");
    print("KLINE Body: ${response.body}");
    // list = json.decode(response.body);
    // return list;
  }

  @override
  void dispos() async {
    await stateController.drain();
    stateController?.close();
  }
}

abstract class BaseBloc {
  void dispos();
}
