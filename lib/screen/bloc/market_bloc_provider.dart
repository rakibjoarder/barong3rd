import 'package:crypto_template/screen/bloc/market_bloc.dart';
import 'package:flutter/material.dart';
export 'package:crypto_template/screen/bloc/market_bloc.dart';


class MarketBlocProvider extends InheritedWidget{

  final bloc = MarketBloc();

  MarketBlocProvider({Key key,Widget child}):super(key:key,child:child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  static MarketBloc of(BuildContext context) => (context.inheritFromWidgetOfExactType(MarketBlocProvider) as MarketBlocProvider).bloc;

}